import React, { Fragment, useState, useEffect } from "react";
import GroupLine from "../components/GroupLine";
import ReactPaginate from "react-paginate";
import SearchBar from "../components/SearchBar";
import { toast } from "react-toastify";
import { failConfig, successConfig } from "../helper/toastConfig";
import * as RoleStatusType from "../types/RoleStatus";
import LoadingScreen from "../components/Loading";
import {
  createGroup,
  deleteGroup,
  editGroup,
  getAllExternal,
  getAllGroup,
  getAllUser,
} from "../api/audience";
import swal, { getConfirmConfig } from "../components/Alert";
import { AUDCOLOR } from "../types/ColorStatus";
import NoData from "../components/NoData";

export default function GroupAudience() {
  const [currentPage, setCurrentPage] = useState(0);
  const [itemOffset, setItemOffset] = useState(0);
  const [pageCount, setPageCount] = useState(0);
  const [itemsPerPage, setItemPerPage] = useState(5);
  const [groupList, setGroupList] = useState([]);
  const [totalPage, setTotalPage] = useState(0);
  const [keyword, setKeyword] = useState("");
  const [onSearch, setOnSearch] = useState(false);
  const [loading, setLoading] = useState(true);
  const [updateCount, setUpdateCount] = useState(0);
  const [localData, setLocalData] = useState({
    availableNames: {
      personOptions: [
        { value: "person 1", label: "Person 1" },
        { value: "person 2", label: "Person 2" },
      ],
      groupOptions: [{ value: "2", label: "Group", color: "#36B37E" }],
      externalOptions: [{ value: "5", label: "External", color: "#00B8D9" }],
    },
  });

  useEffect(() => {
    return populateData(itemsPerPage, itemOffset);
  }, [itemOffset, itemsPerPage, onSearch]);

  const populateData = (itemsPerPage, itemOffset) => {
    let isSubscribed = true;
    setLoading(true);

    const fetchData = async () => {
      const [user, group] = await Promise.all([
        getAllUser(),
        getAllGroup(itemsPerPage, itemOffset, keyword)
      ]);
      if (isSubscribed) {
        const optionsMapping = (e, kind) => ({
          value: e._id,
          label: e.name,
          color: AUDCOLOR[kind],
        });
        const grp = group?.data?.map((d) => {
          return {
            ...d,
            id: d._id,
          };
        });
        const initialLocalData = {
          availableNames: {
            personOptions: user?.data?.map((e) => optionsMapping(e, "person")) || [],
          },
        };

        const npage = Math.ceil(group?.total / itemsPerPage);
        setGroupList([...grp]);
        setLocalData(initialLocalData)
        setPageCount(npage);
        setTotalPage(group?.total);
        setOnSearch(false);
        handleInvalidPage(npage);

        setLoading(false);
      }
    };

    fetchData().catch(console.error);

    // cancel any future `setData`
    return () => (isSubscribed = false);
  };

  async function refreshPage() {
    if (itemOffset === 0) {
      await populateData(itemsPerPage, itemOffset);
    } else {
      setItemOffset(0);
    }
    setCurrentPage(0);
  }

  function groupLineChanged(line) {
    const lines = groupList.map((l) => {
      if (l.id === line.id) {
        return { ...line };
      } else {
        return l;
      }
    });
    setGroupList(lines);
  }

  async function groupLineDeleted(id) {
    try {
      await deleteGroup(id);
      await populateData(itemsPerPage, itemOffset);
      toast.success("Group audiences deleted successfully", successConfig);
    } catch (error) {
      const msg = error?.response?.data?.msg;
      if (msg) {
        toast.error(msg, failConfig);
      } else {
        toast.error("Group audiences cannot be deleted", failConfig);
      }
    }
  }

  async function groupLineUpdated(id) {
    try {
      const line = groupList.filter((l) => l.id === id)[0];
      await editGroup(line, id);

      toast.success("Group audiences updated successfully", successConfig);
    } catch (error) {
      const msg = error?.response?.data?.msg;
      if (msg) {
        toast.error(msg, failConfig);
      } else {
        toast.error("Group audiences cannot be updated", failConfig);
      }
    }
  }

  async function groupLineAdded() {
    try {
      if (itemOffset !== 0) {
        const result = await swal.fire(
          getConfirmConfig(
            "Adding Group?",
            "You will be redirected to first page after adding data",
            "info"
          )
        );
        if (!result.isConfirmed) {
          return;
        }
      }

      const newGroup = {
        id: "",
        name: `NewGroup${Math.floor(Math.random(0, 1) * 1000)}`,
        description: "",
        members: [],
      };

      const data = await createGroup(newGroup);
      newGroup.id = data.id;
      setGroupList([newGroup, ...groupList]);
      await refreshPage();
    } catch (error) {
      const msg = error?.response?.data?.msg;
      if (msg) {
        toast.error(msg, failConfig);
      } else {
        toast.error("Group audiences cannot be created", failConfig);
      }
    }
  }

  function groupLineModeChanged(inc) {
    setUpdateCount(updateCount + inc);
  }

  // Invoke when user click to request another page.
  const handlePageClick = (event) => {
    setCurrentPage(event.selected); // harus set manual, jk tdk maka ga bisa forcepage
    const newOffset = (event.selected * itemsPerPage) % totalPage;
    // console.log(
    //   `User requested page number ${event.selected}, which is offset ${newOffset}`
    // );
    setItemOffset(newOffset);
  };

  const handleSearch = (keyword) => {
    setKeyword(keyword);
    setCurrentPage(0);
    setItemOffset(0);
    setOnSearch(true);
  };

  const handleInvalidPage = (npage) => {
    if (currentPage && currentPage > npage - 1) {
      const newCurrentPage = currentPage - 1;
      setCurrentPage(newCurrentPage);
      const newOffset = (newCurrentPage * itemsPerPage) % npage;
      setItemOffset(newOffset);
    }
  };

  return (
    <Fragment>
      <div className="container-fluid px-0">
        <div className="row mb-1 mx-sm-4 d-flex justify-content-between">
          <h4 className="mt-2 grouptext" style={{ marginLeft: 2 }}>
            Group Audiences
          </h4>
          <div
            className="col-sm-12 col-md-5 my-2 px-0 d-flex align-items-end 
          justify-content-md-end justify-content-sm-start"
          >
            <SearchBar
              setKeyword={handleSearch}
              placeholder="Find audience..."
            />
          </div>
        </div>
        <div className="d-flex flex-column mx-sm-4 mb-4 meev-input-list">
          <div className="col-12 px-0 header-list">
            <div className="row mx-0 col-11">
              <div className="col-4 column-list">Name</div>
              <div className="col-4 column-list">Description</div>
              <div className="col-4 column-list">Members</div>
            </div>
          </div>

          {groupList?.map((object, i) => (
            <GroupLine
              id={object.id}
              name={object.name}
              description={object.description}
              members={object.members}
              key={object.id}
              onChange={groupLineChanged}
              onDelete={groupLineDeleted}
              onUpdate={groupLineUpdated}
              availableName={localData?.availableNames?.personOptions}
              editMode={object.isEdit}
              onEditModeChange={groupLineModeChanged}
            />
          ))}
          
          {!loading && (!groupList || groupList.length == 0) && <NoData className={"mt-4"} />}
          <button
            type="button"
            className="col-12 px-0 blank-button text-primary d-flex align-items-center mt-4"
            onClick={groupLineAdded}
          >
            <i className="fas fa-plus" style={{ fontSize: "1em" }}></i>
            <span className="add" style={{ marginLeft: "5px" }}>
              Add new data
            </span>
          </button>
        </div>
      </div>
      <div className="d-flex justify-content-end mb-4 mx-sm-4 px-4">
        <ReactPaginate
          nextLabel="next >"
          onPageChange={handlePageClick}
          pageRangeDisplayed={3}
          marginPagesDisplayed={2}
          pageCount={pageCount}
          previousLabel="< previous"
          pageClassName="page-item"
          pageLinkClassName="page-link"
          previousClassName="page-item"
          previousLinkClassName="page-link"
          nextClassName="page-item"
          nextLinkClassName="page-link"
          breakLabel="..."
          breakClassName="page-item"
          breakLinkClassName="page-link"
          containerClassName="pagination"
          activeClassName="active"
          renderOnZeroPageCount={null}
          forcePage={currentPage}
        />
      </div>
    </Fragment>
  );
}
