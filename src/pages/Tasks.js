import React, { Fragment, useState, useEffect, useContext } from "react";
import SectionBar from "../components/SectionBar";
import * as TasksStatusType from "../types/TasksStatus";
import TasksTableLine from "../components/TasksTableLine";
import { UserContext } from "../data/user";
import ReactPaginate from "react-paginate";
import SearchBar from "../components/SearchBar";
import { toast } from "react-toastify";
import { failConfig, successConfig } from "../helper/toastConfig";
import { editTasks, getAllTasks } from "../api/task";
import NoData from "../components/NoData";
import client from "../api/request";

export default function Tasks() {
  const [currentPage, setCurrentPage] = useState();
  const [itemOffset, setItemOffset] = useState(0);
  const [pageCount, setPageCount] = useState(0);
  const [itemsPerPage, setItemPerPage] = useState(5);
  const [taskList, setTaskList] = useState([]);
  const [totalPage, setTotalPage] = useState(0);
  const [keyword, setKeyword] = useState("");
  const [onSearch, setOnSearch] = useState(false);
  const [loading, setLoading] = useState(true);

  const user = useContext(UserContext);

  useEffect(() => {
    return populateData(itemsPerPage, itemOffset);
  }, [itemOffset, itemsPerPage, onSearch]);

  const populateData = (itemsPerPage, itemOffset) => {
    // https://devtrium.com/posts/async-functions-useeffect
    let isSubscribed = true;
    setLoading(true);

    const fetchData = async () => {
      const data = await getAllTasks(itemsPerPage, itemOffset, keyword);
      if (isSubscribed) {
        const task = data?.data?.map((d) => {
          return {
            ...d,
            id: d._id,
            evidence: d.evidence || "",
            approved: d.approved || false,
            status: d.status || TasksStatusType.NY,
          };
        });

        const npage = Math.ceil(data?.total / itemsPerPage);
        setTaskList([...task]);
        setPageCount(npage);
        setTotalPage(data?.total);
        setOnSearch(false);
        handleInvalidPage(npage);

        setLoading(false);
      }
    };

    fetchData().catch(console.error);

    // cancel any future `setData`
    return () => (isSubscribed = false);
  };

  function taskListLineChanged(line) {
    const lines = taskList.map((l) => {
      if (l.id === line.id) {
        return { ...line };
      } else {
        return l;
      }
    });
    setTaskList(lines);
  }

  async function taskListLineUpdated(line) {
    try {
      // console.log(line);
      await editTasks(line, line.id);
      toast.success("Task updated successfully", successConfig);
      return true;
    } catch (error) {
      const msg = error?.response?.data?.msg;
      if (msg) {
        toast.error(msg, failConfig);
      } else {
        toast.error("Task cannot be updated", failConfig);
      }
      return false;
    }
  }

  // Invoke when user click to request another page.
  const handlePageClick = (event) => {
    setCurrentPage(event.selected); // harus set manual, jk tdk maka ga bisa forcepage
    const newOffset = (event.selected * itemsPerPage) % totalPage;
    setItemOffset(newOffset);
  };

  const handleSearch = (keyword) => {
    setKeyword(keyword);
    setCurrentPage(0);
    setItemOffset(0);
    setOnSearch(true);
  };

  const handleInvalidPage = (npage) => {
    if (currentPage && currentPage > npage - 1) {
      const newCurrentPage = currentPage - 1;
      setCurrentPage(newCurrentPage);
      const newOffset = (newCurrentPage * itemsPerPage) % npage;
      setItemOffset(newOffset);
    }
  };

  return (
    <Fragment>
      <SectionBar title="Tasks"></SectionBar>
      <div className="wrapper">
        <div className="base-content">
          <div className="container-fluid px-4">
            <div className="row spacing-group mb-1 mx-sm-4 d-flex justify-content-between">
              <h4 className="title-group col-sm-12 col-md-4 my-2 d-flex align-items-center">
                Task List
              </h4>
              <div
                className="col-sm-12 col-md-5 my-2 
                d-flex align-items-center justify-content-md-end justify-content-sm-start"
              >
                <SearchBar
                  setKeyword={handleSearch}
                  placeholder="Find task..."
                />
              </div>
            </div>
            <div className="d-flex flex-column mx-sm-4 mb-4 meev-input-list2">
              <div className="col-12 px-0 header-list">
                <div className="row mx-0 w-100">
                  <div className="row mx-0 px-4" style={{ flexGrow: 1 }}>
                    <div className="col-2 column-list">EVENT</div>
                    <div className="col-2 col-xxl-3 column-list">TASK</div>
                    <div className="col-1 col-xxl-2 column-list">PIC</div>
                    <div className="col-2 column-list">DUE</div>
                    <div className="col-2 col-xxl-1 column-list">EVIDENCE</div>
                    <div className="col-2 col-xxl-1 column-list">STATUS</div>
                    <div className="col-1 column-list d-flex justify-content-center">
                      APPROVED
                    </div>
                  </div>
                  <div className="row pr-1 mr-2" style={{ width: 30 }}></div>
                </div>
              </div>

              {taskList.map(
                (
                  {
                    id,
                    event,
                    mid,
                    name,
                    task,
                    evidence,
                    status,
                    due,
                    approved,
                    creator,
                  },
                  i
                ) => (
                  <TasksTableLine
                    id={id}
                    event={event}
                    mid={mid}
                    name={name}
                    task={task}
                    evidence={evidence}
                    due={due}
                    approved={approved}
                    status={status}
                    creator={creator}
                    key={id}
                    onChange={taskListLineChanged}
                    onUpdate={taskListLineUpdated}
                    user={user}
                    baseurl={client.defaults.baseURL}
                  />
                )
              )}

              {!loading && (!taskList || taskList.length == 0) && (
                <NoData className={"mt-4"} />
              )}
            </div>
          </div>
          <div className="d-flex justify-content-end mb-4 mx-sm-4 px-4">
            <ReactPaginate
              nextLabel="next >"
              onPageChange={handlePageClick}
              pageRangeDisplayed={3}
              marginPagesDisplayed={2}
              pageCount={pageCount}
              previousLabel="< previous"
              pageClassName="page-item"
              pageLinkClassName="page-link"
              previousClassName="page-item"
              previousLinkClassName="page-link"
              nextClassName="page-item"
              nextLinkClassName="page-link"
              breakLabel="..."
              breakClassName="page-item"
              breakLinkClassName="page-link"
              containerClassName="pagination"
              activeClassName="active"
              renderOnZeroPageCount={null}
              forcePage={currentPage}
            />
          </div>
        </div>
      </div>
      <span
        style={{
          fontSize: "0.75em",
          display: "block",
          textAlign: "right",
          marginRight: "10px",
          marginBottom: "5px",
        }}
      >
        Meev@2020
      </span>
    </Fragment>
  );
}
