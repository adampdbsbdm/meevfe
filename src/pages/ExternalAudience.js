import React, { Fragment, useState, useEffect } from "react";
import ExternalLine from "../components/ExternalLine";
import ReactPaginate from "react-paginate";
import SearchBar from "../components/SearchBar";
import { toast } from "react-toastify";
import { failConfig, successConfig } from "../helper/toastConfig";
import LoadingScreen from "../components/Loading";
import {
  createExternal,
  deleteExternal,
  editExternal,
  getAllExternal,
} from "../api/audience";
import swal, { getConfirmConfig } from "../components/Alert";
import NoData from "../components/NoData";

export default function ExternalAudience() {
  const [currentPage, setCurrentPage] = useState(0);
  const [itemOffset, setItemOffset] = useState(0);
  const [pageCount, setPageCount] = useState(0);
  const [itemsPerPage, setItemPerPage] = useState(5);
  const [externalList, setExternalList] = useState([]);
  const [totalPage, setTotalPage] = useState(0);
  const [keyword, setKeyword] = useState("");
  const [onSearch, setOnSearch] = useState(false);
  const [loading, setLoading] = useState(true);
  const [updateCount, setUpdateCount] = useState(0);

  useEffect(() => {
    return populateData(itemsPerPage, itemOffset);
  }, [itemOffset, itemsPerPage, onSearch]);

  const populateData = (itemsPerPage, itemOffset) => {
    // https://devtrium.com/posts/async-functions-useeffect
    let isSubscribed = true;
    setLoading(true);

    const fetchData = async () => {
      const data = await getAllExternal(itemsPerPage, itemOffset, keyword);
      if (isSubscribed) {
        const ext = data?.data?.map((d) => {
          return {
            ...d,
            id: d._id,
          };
        });

        const npage = Math.ceil(data?.total / itemsPerPage);
        setExternalList([...ext]);
        setPageCount(npage);
        setTotalPage(data?.total);
        setOnSearch(false);
        handleInvalidPage(npage);

        setLoading(false);
      }
    };

    fetchData().catch(console.error);

    // cancel any future `setData`
    return () => (isSubscribed = false);
  };

  async function refreshPage() {
    if (itemOffset === 0) {
      await populateData(itemsPerPage, itemOffset);
    } else {
      setItemOffset(0);
    }
    setCurrentPage(0);
  }

  function externalLineChanged(line) {
    const lines = externalList.map((l) => {
      if (l.id === line.id) {
        return { ...line };
      } else {
        return l;
      }
    });
    setExternalList(lines);
  }

  async function externalLineDeleted(id) {
    try {
      await deleteExternal(id);
      await populateData(itemsPerPage, itemOffset);
      toast.success("External audiences deleted successfully", successConfig);
    } catch (error) {
      const msg = error?.response?.data?.msg;
      if (msg) {
        toast.error(msg, failConfig);
      } else {
        toast.error("External audiences cannot be deleted", failConfig);
      }
    }
  }

  async function externalLineUpdated(id) {
    try {
      const line = externalList.filter((l) => l.id === id)[0];
      await editExternal(line, id);
      // await populateData(itemsPerPage, itemOffset);

      // const extlines = externalList.map((l) => {
      //   if (l.id === id) {
      //     return { ...l, anyUpdate:false, isEdit: false };
      //   } else {
      //     return l;
      //   }
      // });
      // setExternalList(extlines);

      toast.success("External audiences updated successfully", successConfig);
    } catch (error) {
      const msg = error?.response?.data?.msg;
      if (msg) {
        toast.error(msg, failConfig);
      } else {
        toast.error("External audiences cannot be updated", failConfig);
      }
    }
  }

  async function externalLineAdded() {
    try {
      if (itemOffset !== 0) {
        const result = await swal.fire(
          getConfirmConfig(
            "Adding External?",
            "You will be redirected to first page after adding data",
            "info"
          )
        );
        if (!result.isConfirmed) {
          return;
        }
      }

      const newExtern = {
        id: "",
        name: `NewPerson${Math.floor(Math.random(0, 1) * 1000)}`,
        information: "",
        contact: "",
        email: "",
      };

      const data = await createExternal(newExtern);
      newExtern.id = data.id;
      setExternalList([newExtern, ...externalList]);
      await refreshPage();
    } catch (error) {
      const msg = error?.response?.data?.msg;
      if (msg) {
        toast.error(msg, failConfig);
      } else {
        toast.error("External audiences cannot be created", failConfig);
      }
    }
  }

  function externalLineModeChanged(inc) {
    setUpdateCount(updateCount + inc);
  }

  // Invoke when user click to request another page.
  const handlePageClick = (event) => {
    setCurrentPage(event.selected); // harus set manual, jk tdk maka ga bisa forcepage
    const newOffset = (event.selected * itemsPerPage) % totalPage;
    // console.log(
    //   `User requested page number ${event.selected}, which is offset ${newOffset}`
    // );
    setItemOffset(newOffset);
  };

  const handleSearch = (keyword) => {
    setKeyword(keyword);
    setCurrentPage(0);
    setItemOffset(0);
    setOnSearch(true);
  };

  const handleInvalidPage = (npage) => {
    if (currentPage && currentPage > npage - 1) {
      const newCurrentPage = currentPage - 1;
      setCurrentPage(newCurrentPage);
      const newOffset = (newCurrentPage * itemsPerPage) % npage;
      setItemOffset(newOffset);
    }
  };

  return (
    <Fragment>
      <div className="container-fluid px-0">
        <div className="row mb-1 mx-sm-4 d-flex justify-content-between">
          <h4 className="mt-2 grouptext" style={{marginLeft:2}}>External Audiences</h4>
          <div
            className="col-sm-12 col-md-5 my-2 px-0 d-flex align-items-end 
          justify-content-md-end justify-content-sm-start"
          >
            <SearchBar
              setKeyword={handleSearch}
              placeholder="Find audience..."
            />
          </div>
        </div>
        <div className="d-flex flex-column mx-sm-4 mb-4 meev-input-list">
          <div className="col-12 px-0 header-list">
            <div className="row mx-0 col-11">
              <div className="col-3 column-list">Name</div>
              <div className="col-3 column-list">Information</div>
              <div className="col-3 column-list">Contact</div>
              <div className="col-3 column-list">Email</div>
            </div>
          </div>
          {externalList?.map((object, i) => (
            <ExternalLine
              id={object.id}
              name={object.name}
              information={object.information}
              contact={object.contact}
              email={object.email}
              showUpdate={object.anyUpdate}
              key={object.id}
              onChange={externalLineChanged}
              onDelete={externalLineDeleted}
              onUpdate={externalLineUpdated}
              editMode={object.isEdit}
              onEditModeChange={externalLineModeChanged}
            />
          ))}
          
          {!loading && (!externalList || externalList.length == 0) && <NoData className={"mt-4"} />}
          <button
            type="button"
            className="col-12 px-0 blank-button text-primary d-flex align-items-center mt-4"
            onClick={externalLineAdded}
          >
            <i className="fas fa-plus" style={{ fontSize: "1em" }}></i>
            <span className="add" style={{ marginLeft: "5px" }}>
              Add new data
            </span>
          </button>
        </div>
      </div>
      <div className="d-flex justify-content-end mb-4 mx-sm-4 px-4">
        <ReactPaginate
          nextLabel="next >"
          onPageChange={handlePageClick}
          pageRangeDisplayed={3}
          marginPagesDisplayed={2}
          pageCount={pageCount}
          previousLabel="< previous"
          pageClassName="page-item"
          pageLinkClassName="page-link"
          previousClassName="page-item"
          previousLinkClassName="page-link"
          nextClassName="page-item"
          nextLinkClassName="page-link"
          breakLabel="..."
          breakClassName="page-item"
          breakLinkClassName="page-link"
          containerClassName="pagination"
          activeClassName="active"
          renderOnZeroPageCount={null}
          forcePage={currentPage}
        />
      </div>
    </Fragment>
  );
}
