import React, { Fragment, useState, useEffect } from "react";
import { useHistory, useParams } from "react-router-dom";
import SectionBar from "../components/SectionBar";
import SimpleTextInput from "../components/SimpleTextInput";
import AreaTextInput from "../components/AreaTextInput";
import LocationTextInput from "../components/LocationTextInput";
import PICLine from "../components/PICLine";
import SimpleTextOutput from "../components/SimpleTextOutput";
import MultiTextOutput from "../components/MultiTextOutput";
import PICDetailsLine from "../components/PICDetailsLine";
import { UserContext } from "../data/user";
import { getMeetingDetails } from "../api/meeting";
import { getDateOnlyString, getDateString } from "../helper/dateparser";
import { AUDCOLOR } from "../types/ColorStatus";
import * as MeetingStatusType from "../types/MeetingStatus";
import NoteDetailsLine from "../components/NoteDetailsLine";
import DecisionDetailsLine from "../components/DecisionDetailsLine";
import client from "../api/request";

export default function MeetingDetails() {
  let { id } = useParams();
  let history = useHistory();

  useEffect(() => {
    // https://devtrium.com/posts/async-functions-useeffect
    let isSubscribed = true;

    const fetchData = async () => {
      try {
        const data = await getMeetingDetails(id);
        if (isSubscribed) {
          console.log(data);
          const local = {
            ...data,
            id: data._id,
            datestart: getDateString(new Date(data.datestart)),
            dateend: getDateString(new Date(data.dateend)),
            attendees:
              data.attendees?.map((a) => {
                return { id: a.aid, name: a.name, color: AUDCOLOR[a.kind] };
              }) || [],
            status: data.status || MeetingStatusType.DONE,
          };

          const pic =
            data?.pic?.map((a) => {
              return { ...a, id: a.aid, name: { id: a.aid, name: a.name } };
            }) || [];

          const decs =
            data?.report?.pic?.map((a) => {
              return { ...a, id: a._id, pic: { id: a.aid, name: a.name } };
            }) || [];

          const notes =
            data?.report?.notes?.map((a) => {
              return { ...a, id: a._id };
            }) || [];

          setLocalData(local);
          setPicList(pic);
          setDecisionList(decs);
          setNoteList(notes);
        }
      } catch (ex) {
        console.log(ex);
        // history.push('/meeting')
      }
    };

    fetchData().catch(console.error);

    // cancel any future `setData`
    return () => (isSubscribed = false);
  }, [id]);

  useEffect(() => {
    console.log(JSON.stringify(localData));
  });

  const [localData, setLocalData] = useState();

  const [picList, setPicList] = useState([]);
  const [decisionList, setDecisionList] = useState([]);
  const [noteList, setNoteList] = useState([]);

  return (
    <Fragment>
      <SectionBar title="Meeting" subtitle="Meeting Details"></SectionBar>
      <div className="wrapper">
        <div className="base-content">
          <div className="container-fluid px-4">
            <h4 className="mt-4 grouptext ml-sm-4">General Information</h4>
            <div className="d-flex flex-column ml-sm-4">
              <div className="row col-lg-12 col-xl-8 mt-4 px-0">
                <SimpleTextOutput
                  caption="Event"
                  value={localData?.event}
                ></SimpleTextOutput>
              </div>
              <div className="row col-lg-12 col-xl-8 mt-4 px-0">
                <SimpleTextOutput
                  caption="Leader"
                  value={localData?.leader?.name}
                ></SimpleTextOutput>
              </div>
              {localData?.locoffline ? (
                <div className="row col-lg-12 col-xl-8 mt-4 px-0">
                  <SimpleTextOutput
                    caption="Location"
                    value={localData?.location_offline}
                  ></SimpleTextOutput>
                </div>
              ) : (
                ""
              )}
              {localData?.loconline ? (
                <div className="row col-lg-12 col-xl-8 mt-4 px-0">
                  <SimpleTextOutput
                    caption="Online URL"
                    value={localData?.location_online}
                    link="true"
                  ></SimpleTextOutput>
                </div>
              ) : (
                ""
              )}
              <div className="row col-lg-12 col-xl-8 mt-4 px-0">
                <SimpleTextOutput
                  caption="Start"
                  value={localData?.datestart}
                ></SimpleTextOutput>
              </div>
              <div className="row col-lg-12 col-xl-8 mt-4 px-0">
                <SimpleTextOutput
                  caption="End"
                  value={localData?.dateend}
                ></SimpleTextOutput>
              </div>
              <div className="row col-lg-12 col-xl-8 mt-4 px-0">
                <SimpleTextOutput
                  caption="Agenda"
                  value={localData?.agenda}
                ></SimpleTextOutput>
              </div>
              <div className="row col-lg-12 col-xl-8 mt-4 px-0">
                <MultiTextOutput
                  caption="Invitees"
                  value={localData?.attendees?.map((a) => a.name)}
                ></MultiTextOutput>
              </div>
            </div>

            <h4 className="mt-4 grouptext ml-sm-4">PIC List</h4>
            <div className="d-flex flex-column mx-sm-4 mb-4 meev-input-list">
              <div className="col-12 px-0 header-list">
                <div className="row mx-0 col-12">
                  <div className="col-4 column-list">Name</div>
                  <div className="col-4 column-list">Task</div>
                  <div className="col-4 column-list">Due</div>
                </div>
              </div>

              {picList?.map((object, i) => (
                <PICDetailsLine
                  id={object.id}
                  name={object.name.name}
                  task={object.task}
                  due={getDateString(new Date(object.due))}
                  key={i}
                />
              ))}
            </div>

            <h4 className="mt-4 grouptext ml-sm-4">Attachment</h4>
            <div className="d-flex flex-column ml-sm-4">
              <div className="row col-lg-12 col-xl-8 mt-4 px-0">
                {localData?.attachment ? (
                  <div className="d-flex align-items-center justify-content-start col-12">
                    <a
                      href={`${client.defaults.baseURL}${localData?.attachment}`}
                      target="_blank"
                      className="d-flex align-items-center text-primary"
                    >
                      <i
                        className="fas fa-download"
                        style={{ fontSize: "1em", marginRight: 5 }}
                      ></i>
                      <span> Download</span>
                    </a>
                  </div>
                ) : (
                  <span className="col-12">None</span>
                )}
              </div>
            </div>

            <div style={{ marginBottom: 75 }}></div>
          </div>
        </div>
      </div>

      {localData?.report && (
        <Fragment>
          <div className="wrapper">
            <div className="base-content">
              <div className="container-fluid px-4">
                <h4 className="mt-4 grouptext ml-sm-4">Report Information</h4>
                <div className="d-flex flex-column ml-sm-4">
                  <div className="row col-lg-12 col-xl-8 mt-4 px-0">
                    <MultiTextOutput
                      caption="Attendees"
                      value={localData?.report?.attendees?.map((a) => a.name)}
                    ></MultiTextOutput>
                  </div>
                </div>

                <h4 className="mt-4 grouptext ml-sm-4">Note List</h4>
                <div className="d-flex flex-column mx-sm-4 mb-4 meev-input-list">
                  <div className="col-12 px-0 header-list">
                    <div className="row mx-0 col-12">
                      <div className="col-4 column-list">Point/Case</div>
                      <div className="col-8 column-list">Notes</div>
                    </div>
                  </div>

                  {noteList?.map((object, i) => (
                    <NoteDetailsLine
                      id={object._id}
                      notes={object.notes}
                      point={object.point}
                      key={i}
                    />
                  ))}
                </div>

                <h4 className="mt-4 grouptext ml-sm-4">Decision List</h4>
                <div className="d-flex flex-column mx-sm-4 mb-4 meev-input-list">
                  <div className="col-12 px-0 header-list">
                    <div className="row mx-0 col-12">
                      <div className="col-3 column-list">PIC</div>
                      <div className="col-3 column-list">Decision</div>
                      <div className="col-3 column-list">Detail</div>
                      <div className="col-3 column-list">Due</div>
                    </div>
                  </div>

                  {decisionList?.map((object, i) => (
                    <DecisionDetailsLine
                      id={object._id}
                      pic={object.name}
                      decision={object.decision}
                      detail={object.detail}
                      due={getDateOnlyString(new Date(object.due))}
                      key={i}
                    />
                  ))}
                </div>

                <h3 className="mt-4 grouptext ml-sm-4">Report Attachment</h3>
                <div className="d-flex flex-column ml-sm-4">
                  <div className="row col-lg-12 col-xl-8 mt-4 px-0">
                    {localData?.report?.attachment ? (
                      <div className="d-flex align-items-center justify-content-start col-12">
                        <a
                          href={localData?.report?.attachment}
                          target="_blank"
                          className="d-flex align-items-center text-primary"
                        >
                          <i
                            className="fas fa-download"
                            style={{ fontSize: "1em", marginRight: 5 }}
                          ></i>
                          <span> Download</span>
                        </a>
                      </div>
                    ) : (
                      <span className="col-12">None</span>
                    )}
                  </div>
                </div>
                <div style={{ marginBottom: 75 }}></div>
              </div>
            </div>
          </div>
        </Fragment>
      )}
      <span
        style={{
          fontSize: "0.75em",
          display: "block",
          textAlign: "right",
          marginRight: "10px",
          marginBottom: "5px",
        }}
      >
        Meev@2020
      </span>
    </Fragment>
  );
}

// useEffect(() => {
//   // Fetch from API
//   setLocalData({
//     event: "Regulasi frekuensi",
//     leader: "Budi",
//     agenda: "Pembahasan regulasi",
//     offlineloc: "Gedung A",
//     onlineloc: "https://meeet.com/ava2as21kfa",
//     attachment: "https://www.7-zip.org/a/7za920.zip",
//     datetime: "Fri Dec 10 2020 15:43:46 GMT+0700 (Indochina Time)",
//     availableNames: {
//       personOptions: [
//         { value: "person 1", label: "Person 1", color: "#4B4B4D" },
//         { value: "person 2", label: "Person 2", color: "#4B4B4D" },
//         { value: "person 3", label: "Person 3", color: "#4B4B4D" },
//       ],
//       groupOptions: [{ value: "2", label: "Group", color: "#36B37E" }],
//       externalOptions: [{ value: "5", label: "External", color: "#00B8D9" }],
//     },
//     attendees: [
//       { id: "person 1", name: "Person 1", color: "#4B4B4D" },
//       { id: "person 2", name: "Person 2", color: "#4B4B4D" },
//       { id: "person 3", name: "Person 3", color: "#4B4B4D" },
//       { id: "2", name: "Group", color: "#36B37E" },
//       { id: "5", name: "External", color: "#00B8D9" },
//     ],
//   });

//   setPicList([
//     {
//       id: "1",
//       name: {
//         id: "person 1",
//         name: "Person 1",
//       },
//       task: "task 1",
//       description: "detail task 1",
//     },
//     {
//       id: "2",
//       name: {
//         id: "person 2",
//         name: "Person 2",
//       },
//       task: "task 2",
//       description: "detail task 2",
//     },
//     {
//       id: "3",
//       name: {
//         id: "person 3",
//         name: "Person 3",
//       },
//       task: "task 2",
//       description: "detail task 3",
//     },
//   ]);
// }, []);
