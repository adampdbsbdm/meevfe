import React, {
  Fragment,
  useState,
  useEffect,
  useRef,
  useContext,
} from "react";
import { useHistory } from "react-router-dom";
import SectionBar from "../components/SectionBar";
// import SummaryCard from "../components/SummaryCard";
import SummaryCardSimp from "../components/SummaryCardSimp";
import crown from "../assets/crown-solid.svg";
import clipboard from "../assets/clipboard-solid.svg";
import business from "../assets/business-time-solid.svg";
import filesig from "../assets/file-signature-solid.svg";
import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import timeGridPlugin from "@fullcalendar/timegrid";
import interactionPlugin from "@fullcalendar/interaction";
import {
  INVITATION,
  LEADER,
  MEETING,
  PIC,
  TASK,
} from "../types/ActivityStatus";
import AdaptiveWithDetector from "../components/AdaptiveWithDetector";
import { UserContext } from "../data/user";
import { getAllDashboard } from "../api/dashboard";
import {
  DASHCOLOR,
  invitationColor,
  leaderColor,
  meetingColor,
  picColor,
  taskColor,
} from "../types/ColorStatus";
import { MEMBER } from "../types/RoleStatus";

function Dashboard() {
  const calendar = useRef(null);

  const user = useContext(UserContext);

  useEffect(() => {
    return populateDashboard(new Date().toISOString());
  }, []);

  const populateDashboard = (date) => {
    let isSubscribed = true;

    const fetchData = async () => {
      try {
        const dashboard = await getAllDashboard(date);

        if (isSubscribed) {
          const initialEventData = {
            ...dashboard,
            events: dashboard?.events?.map((e) => ({
              ...e,
              start: e.start, //.replace('Z', "+07:00"),
              end: e.end, //.replace('Z', "+07:00"),
              type: e.type || MEETING,
              color: DASHCOLOR[(e.type || MEETING).toLowerCase()],
            })),
          };

          setEventList(initialEventData);
        }
      } catch (ex) {
        console.log(ex);
      }
    };

    fetchData().catch(console.error);

    // cancel any future `setData`
    return () => (isSubscribed = false);
  };

  let history = useHistory();
  const [eventList, setEventList] = useState({});

  const handleDateClick = (arg) => {
    // console.log(user);
    if (
      user?.privilege &&
      user?.privilege?.toLowerCase() !== MEMBER.toLowerCase()
    ) {
      console.log(arg.dateStr);
      if (arg.dateStr.indexOf("T") === -1) {
        arg.dateStr += "T08:00:00+07:00";
      }
      history.push("/meeting/create/" + arg.dateStr);
    }
  };

  const handleEventClick = (arg) => {
    if (arg.event.extendedProps.type !== TASK) {
      history.push("/meeting/detail/" + arg.event.id);
    } else {
      history.push("/tasks");
    }
  };

  const forceRefreshCalendar = () => {
    const api = calendar.current.getApi();
    api.updateSize();
  };

  return (
    <Fragment>
      <SectionBar title="Dashboard"></SectionBar>
      <div className="wrapper mt-4">
        <div className="base-content" style={{ backgroundColor: "#FFFFFF00" }}>
          <div className="container-fluid px-4">
            <div className="d-flex flex-column mx-sm-4 mb-4">
              {user && (
                <div className="col-12 px-0 row mx-0 mb-4 justify-content-around">
                  <div className="col-xs-12 col-md-6 col-xl-3 mb-md-3 mb-sm-3">
                    <SummaryCardSimp
                      caption="Upcoming Meeting Leader"
                      value={eventList.summary?.leader || 0}
                      color={leaderColor}
                      image={crown}
                    ></SummaryCardSimp>
                  </div>
                  <div className="col-xs-12 col-md-6 col-xl-3 mb-md-3 mb-sm-3">
                    <SummaryCardSimp
                      caption="Upcoming Meeting Invitation"
                      value={eventList.summary?.invitation || 0}
                      color={invitationColor}
                      image={business}
                      mcorr={3}
                    ></SummaryCardSimp>
                  </div>
                  <div className="col-sm-12 col-md-6 col-xl-3 mb-md-3 mb-sm-3">
                    <SummaryCardSimp
                      caption="Appointed PIC Agenda"
                      value={eventList.summary?.pic || 0}
                      color={picColor}
                      image={filesig}
                      mcorr={5}
                    ></SummaryCardSimp>
                  </div>
                  {/* <div className="col-sm-12 col-md-6 col-xl-3 mb-md-3 mb-sm-3">
                    <SummaryCardSimp
                      caption="Tasks To Do"
                      value={eventList.summary?.task || 0}
                      color={taskColor}
                      image={clipboard}
                    ></SummaryCardSimp>
                  </div> */}
                  <div className="col-sm-12 col-md-6 col-xl-3 mb-md-3 mb-sm-3">
                    <SummaryCardSimp
                      caption="Upcoming Meeting"
                      value={eventList.summary?.meeting || 0}
                      color={meetingColor}
                      image={clipboard}
                    ></SummaryCardSimp>
                  </div>
                </div>
              )}
              {/* <div className="col-12 px-0 row mx-0 mb-4 justify-content-around">
                <div className="col-xs-12 col-md-6 col-xl-3 mb-md-3 mb-sm-3">
                  <SummaryCard
                    caption="Meeting Leader"
                    value={eventList.summary?.leader || 0}
                    color={leaderColor}
                    image={crown}
                  ></SummaryCard>
                </div>
                <div className="col-xs-12 col-md-6 col-xl-3 mb-md-3 mb-sm-3">
                  <SummaryCard
                    caption="Meeting Invitation"
                    value={eventList.summary?.meeting || 0}
                    color={meetingColor}
                    image={business}
                    mcorr={3}
                  ></SummaryCard>
                </div>
                <div className="col-sm-12 col-md-6 col-xl-3 mb-md-3 mb-sm-3">
                  <SummaryCard
                    caption="PIC Agenda"
                    value={eventList.summary?.pic || 0}
                    color={picColor}
                    image={filesig}
                    mcorr={5}
                  ></SummaryCard>
                </div>
                <div className="col-sm-12 col-md-6 col-xl-3 mb-md-3 mb-sm-3">
                  <SummaryCard
                    caption="Tasks To Do"
                    value={eventList.summary?.task || 0}
                    color={taskColor}
                    image={clipboard}
                  ></SummaryCard>
                </div>
              </div> */}

              {/* <div className="card p-4">
                <AdaptiveWithDetector callback={forceRefreshCalendar}>
                  <FullCalendar
                    key={"asdvlaksgjasldh"}
                    ref={calendar}
                    plugins={[dayGridPlugin, interactionPlugin]}
                    initialView="dayGridMonth"
                    dateClick={handleDateClick}
                    eventClick={handleEventClick}
                    events={eventList.event || []}
                  />
                </AdaptiveWithDetector>
              </div> */}

              {/* // karena data dalam Z, perlu set timeZone=local agar auto parsing ke +7 
              aslinya ga perlu krn defaultnya local, tapi lib react kyknya ga ada defaultnya*/}
              <div className="card p-4">
                <AdaptiveWithDetector callback={forceRefreshCalendar}>
                  <FullCalendar
                    timeZone="local" 
                    key={"asdvlaksgjasldh"}
                    ref={calendar}
                    plugins={[dayGridPlugin, timeGridPlugin, interactionPlugin]}
                    initialView="timeGridWeek"
                    dateClick={handleDateClick}
                    eventClick={handleEventClick}
                    events={eventList.events || []}
                    headerToolbar={{
                      left: "prev,next today",
                      center: "title",
                      right:
                        "dayGridMonth,timeGridTwoWeeks,timeGridWeek,timeGridDay",
                    }}
                    dayMaxEvents={true}
                    views={{
                      timeGridTwoWeeks: {
                        type: "timeGrid",
                        duration: { days: 14 },
                        buttonText: "2 weeks",
                      },
                    }}
                    // events="https://fullcalendar.io/api/demo-feeds/events.json"
                  />
                </AdaptiveWithDetector>
              </div>
            </div>
          </div>
        </div>
      </div>
      <span
        style={{
          fontSize: "0.75em",
          display: "block",
          textAlign: "right",
          marginRight: "10px",
          marginBottom: "5px",
        }}
      >
        Meev@2020
      </span>
    </Fragment>
  );
}

export default Dashboard;

// useEffect(() => {
//   // API Fetch
//   setEventList({
//     summary: {
//       leader: 1,
//       invitation: 2,
//       pic: 1,
//       task: 4,
//     },
//     events: [
//       {
//         id: "1",
//         type: INVITATION,
//         title: "event 1",
//         start: "2022-04-01T12:30:00",
//         end: "2022-04-01T13:30:00",
//         color: DASHCOLOR[INVITATION.toLowerCase()],
//       },
//       {
//         id: "2",
//         type: INVITATION,
//         title: "event 2",
//         date: "2022-03-26T13:30:00",
//         color: DASHCOLOR[INVITATION.toLowerCase()],
//       },
//       {
//         id: "1",
//         type: TASK,
//         title: "task name",
//         date: "2022-03-27T18:30:00",
//         color: DASHCOLOR[TASK.toLowerCase()],
//       },
//       {
//         id: "2",
//         type: PIC,
//         title: "task name",
//         date: "2022-03-28",
//         color: DASHCOLOR[PIC.toLowerCase()],
//       },
//       {
//         id: "3",
//         type: LEADER,
//         title: "event",
//         start: "2022-03-25",
//         end: "2022-04-01",
//         color: DASHCOLOR[LEADER.toLowerCase()],
//       },
//     ],
//   });
// }, []);
