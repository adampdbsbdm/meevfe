import React, { useState, useEffect, useRef } from "react";
import Modal from "react-bootstrap/Modal";
import { changePassword } from "../api/user";
import swal, { getConfirmConfig, getErrorConfig } from "../components/Alert";
import SimpleTextInput from "../components/SimpleTextInput";

export default function ChangePassword({ isShow, onChange, onSuccess }) {
  const [show, setShow] = useState(false);
  const [localData, setLocalData] = useState();
  const [wasvalidated, setWasvalidated] = useState(false);
  let mainform = useRef(null);

  useEffect(() => {
    setShow(isShow);
  }, [isShow]);

  function dataChanged(o) {
    setLocalData({ ...localData, [o.key]: o.value });
  }

  function getSubmitData() {
    let data = {
      ...localData,
    };
    // console.log(localData);
    // console.log(data);

    if (data.newpassword !== data.cnewpassword) {
      return swal
        .fire(getErrorConfig("Fail", "Password not match"))
        .then(() => {
          return null;
        });
    }

    return data;
  }

  const handleClose = () => {
    onChange(false);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setWasvalidated(true);
    if (mainform.current.checkValidity()) {
      try {
        const result = await swal.fire(
          getConfirmConfig("Change Password?", "Do you want to change password")
        );
        if (result.isConfirmed) {
          const data = getSubmitData();
          if (data) {
            await changePassword(data);
            resetForm();
            onSuccess();
            handleClose();
          }
        }
      } catch (error) {
        if (error?.response?.data?.errors[0]?.err == 1) {
          return swal
            .fire(getErrorConfig("Fail", "Invalid Password"))
            .then(() => {
              return null;
            });
        }
        console.log("Cannot submit");
      }
      // history.push("/meeting");
    } else {
      console.log("not valid");
    }
  };

  function resetForm() {
    setWasvalidated(false);
    setLocalData({});
  }

  return (
    <>
      <Modal
        centered
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header className="justify-content-between align-items-center border-bottom-0">
          <h4 className="ml-4 my-2" style={{ fontSize: "1.2rem" }}>
            Change Password
          </h4>
          <button
            type="button"
            className="blank-button text-secondary d-flex align-items-center 
            justify-content-center col-1"
            onClick={handleClose}
          >
            <i className="fas fa-times" style={{ fontSize: "1.3em" }}></i>
          </button>
        </Modal.Header>
        <Modal.Body className="py-0 px-0">
          <form
            ref={mainform}
            className={`${wasvalidated ? "was-validated" : ""}`}
            onSubmit={(e) => {
              e.preventDefault();
            }}
            noValidate
          >
            <div
              className="d-flex flex-column mt-4  align-items-center"
              style={{ paddingLeft: "2rem", paddingRight: "2rem" }}
            >
              <div className="row col-lg-12 mt-2 px-0">
                <SimpleTextInput
                  name="oldpassword"
                  caption="Old Password"
                  placeholder="Input old password..."
                  tipe={"password"}
                  icon={"fas fa-key"}
                  spacing={[5, 7]}
                  value={localData?.password || ""}
                  change={dataChanged}
                  required={true}
                ></SimpleTextInput>
              </div>
              <div className="row col-lg-12 mt-3 px-0">
                <SimpleTextInput
                  name="newpassword"
                  caption="New Password"
                  placeholder="Input new password..."
                  tipe={"password"}
                  icon={"fas fa-key"}
                  spacing={[5, 7]}
                  value={localData?.newpassword || ""}
                  change={dataChanged}
                  required={true}
                ></SimpleTextInput>
              </div>
              <div className="row col-lg-12 mt-3 px-0">
                <SimpleTextInput
                  name="cnewpassword"
                  caption="Confirm New Password"
                  placeholder="Confirm new password..."
                  tipe={"password"}
                  icon={"fas fa-key"}
                  spacing={[5, 7]}
                  value={localData?.cnewpassword || ""}
                  change={dataChanged}
                  required={true}
                ></SimpleTextInput>
              </div>
            </div>
          </form>
        </Modal.Body>
        <Modal.Footer
          className="border-top-0 mt-3 mb-3"
          style={{ paddingLeft: "3rem", paddingRight: "3rem" }}
        >
          <button
            type="submit"
            className="mb-4 btn btn-custom btn-primary-custom"
            onClick={handleSubmit}
          >
            Change
          </button>
        </Modal.Footer>
      </Modal>
    </>
  );
}
