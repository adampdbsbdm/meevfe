import React, { useState, useEffect, useRef } from "react";
import Modal from "react-bootstrap/Modal";
import { getAccount, changeAccount } from "../api/user";
import swal, { getConfirmConfig, getErrorConfig } from "../components/Alert";
import PhotoUpload from "../components/PhotoUpload";
import SimpleTextInput from "../components/SimpleTextInput";
import { setUserDataStorageOnly } from "../data/user";

export default function ChangeAccount({ isShow, onChange, onSuccess }) {
  const [show, setShow] = useState(false);
  const [localData, setLocalData] = useState();
  const [wasvalidated, setWasvalidated] = useState(false);
  let mainform = useRef(null);

  useEffect(() => {
    setShow(isShow);
    if(isShow) {
      populateEdit()
    }
  }, [isShow]);

  const populateEdit = () => {
    // https://devtrium.com/posts/async-functions-useeffect
    let isSubscribed = true;
   
    const fetchData = async () => {
      try {
        const user = await getAccount()

        if (isSubscribed) {
          let initialLocalData = {
            ...user,
          };

          setLocalData(initialLocalData);
        }
      } catch (ex) {
        console.log(ex);
      }
    };

    fetchData().catch(console.error);

    // cancel any future `setData`
    return () => (isSubscribed = false);
  };

  function dataChanged(o) {
    setLocalData({ ...localData, [o.key]: o.value });
  }

  function getSubmitData() {
    let data = {
      ...localData,
    };
    return data;
  }

  const handleClose = () => {
    onChange(false);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setWasvalidated(true);
    if (mainform.current.checkValidity()) {
      try {      
          const data = getSubmitData();
          console.log(data);
          if (data) {
            const resp = await changeAccount(data);
            resetForm();
            onSuccess();
            handleClose();
            setUserDataStorageOnly(resp);
          }
      } catch (error) {
        if (error?.response?.data?.errors[0]?.err == 1) {
         
        }
        console.log("Cannot submit");
      }
      // history.push("/meeting");
    } else {
      console.log("not valid");
    }
  };

  function resetForm() {
    setWasvalidated(false);
    setLocalData({});
  }

  return (
    <>
      <Modal
        centered
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header className="justify-content-between align-items-center border-bottom-0">
          <h4 className="ml-4 my-2" style={{ fontSize: "1.2rem" }}>
            Change Account Information
          </h4>
          <button
            type="button"
            className="blank-button text-secondary d-flex align-items-center 
            justify-content-center col-1"
            onClick={handleClose}
          >
            <i className="fas fa-times" style={{ fontSize: "1.3em" }}></i>
          </button>
        </Modal.Header>
        <Modal.Body className="py-0 px-0">
          <form
            ref={mainform}
            className={`${wasvalidated ? "was-validated" : ""}`}
            onSubmit={(e) => {
              e.preventDefault();
            }}
            noValidate
          >
            <div
              className="d-flex flex-column mt-4 align-items-center"
              style={{ paddingLeft: "2rem", paddingRight: "2rem" }}
            >
              <div className="row col-lg-12 mt-2 px-0">
                <SimpleTextInput
                  name="name"
                  caption="Name"
                  placeholder="Input name..."
                  tipe={"text"}
                  spacing={[4, 8]}
                  value={localData?.name || ""}
                  change={dataChanged}
                  required={true}
                ></SimpleTextInput>
              </div>
              <div className="row col-lg-12 mt-3 px-0">
                <SimpleTextInput
                  name="email"
                  caption="Email"
                  placeholder="Input email..."
                  tipe={"email"}
                  spacing={[4, 8]}
                  value={localData?.email || ""}
                  change={dataChanged}
                ></SimpleTextInput>
              </div>
              <div className="row col-lg-12 mt-3 px-0">
                <SimpleTextInput
                  name="contact"
                  caption="Contact"
                  placeholder="Input contact..."
                  tipe={"text"}
                  spacing={[4, 8]}
                  value={localData?.contact || ""}
                  change={dataChanged}
                ></SimpleTextInput>
              </div>
               <div className="row col-lg-12 mt-3 px-0">
                <PhotoUpload
                  name="photo"
                  caption="Photo"
                  spacing={[4, 8]}
                  value={localData?.photo || ""}
                  change={dataChanged}
                ></PhotoUpload>
              </div>
            </div>
          </form>
        </Modal.Body>
        <Modal.Footer
          className="border-top-0 mt-3 mb-3"
          style={{ paddingLeft: "3rem", paddingRight: "3rem" }}
        >
          <button
            type="submit"
            className="btn btn-custom btn-primary-custom"
            onClick={handleSubmit}
          >
            Change
          </button>
        </Modal.Footer>
      </Modal>
    </>
  );
}
