import React, { Fragment, useState, useEffect, useRef } from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import { loginUser } from "../api/user";
import swal, { getErrorConfig } from "../components/Alert";
import SimpleTextInput from "../components/SimpleTextInput";
import SimpleTextInputGroup from "../components/SimpleTextInputGroup";
import { setUserDataStorage } from "../data/user";

export default function Login({ isShow, onChange, onSuccess }) {
  const [show, setShow] = useState(false);
  const [localData, setLocalData] = useState();
  const [wasvalidated, setWasvalidated] = useState(false);
  let mainform = useRef(null);
  let submitbtn = useRef(null);

  useEffect(() => {
    setShow(isShow);
  }, [isShow]);

  function dataChanged(o) {
    setLocalData({ ...localData, [o.key]: o.value });
  }

  function getSubmitData() {
    let data = {
      ...localData,
    };
    // console.log(localData);
    // console.log(data);

    return data;
  }
  const handleClose = () => {onChange(false)};
  const handleSubmit = async (e) => {
    e.preventDefault();
    setWasvalidated(true);
    if (mainform.current.checkValidity()) {
      try {
        const resp = await loginUser(getSubmitData());
        setUserDataStorage(resp);
        onSuccess()
        resetForm()
        handleClose();
        window.location.reload();
      } catch(error) {
        // eslint-disable-next-line
        if (error?.response?.data?.errors[0]?.err == 1) {
          return swal
            .fire(getErrorConfig("Fail", "Invalid Login data"))
            .then(() => {
              return null;
            });
        }
        console.log("Cannot submit");
      }
      // history.push("/meeting");
    } else {
      console.log("not valid");
    }
  };

  function resetForm() {
    setWasvalidated(false)
    setLocalData({})
  }

  return (
    <>
      <Modal
        centered
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={true}
      >
        <Modal.Header className="justify-content-end border-bottom-0">
          <button
            type="button"
            className="blank-button text-secondary d-flex align-items-center 
            justify-content-center col-1"
            onClick={handleClose}
          >
            <i className="fas fa-times" style={{ fontSize: "1.3em" }}></i>
          </button>
        </Modal.Header>
        <Modal.Body className="py-0 px-0">
          <form
            ref={mainform}
            className={`${wasvalidated ? "was-validated" : ""}`}
            onSubmit={(e) => {
              e.preventDefault();
            }}
            noValidate
            onKeyPress={(e) => e.key === "Enter"? submitbtn.current.click() : ""}
          >
            <div
              className="d-flex flex-column mt-2  align-items-center"
              style={{ paddingLeft: "3.5rem", paddingRight: "3.5rem" }}
            >
              <div className="row col-lg-12 px-0 justify-content-center">
                <h4 className="text-center" style={{ fontSize: "1.3rem" }}>
                  Welcome,
                  <br /> sign in to get more feature!
                </h4>
              </div>
              <div className="row col-lg-12 mt-2 px-0">
                <SimpleTextInputGroup
                  name="username"
                  caption="Username"
                  placeholder="Input username..."
                  value={localData?.username || ""}
                  icon={"fas fa-user"}
                  change={dataChanged}
                  required={true}
                ></SimpleTextInputGroup>
              </div>
              <div className="row col-lg-12 mt-2 px-0">
                <SimpleTextInputGroup
                  name="password"
                  caption="Password"
                  placeholder="Input password..."
                  tipe={"password"}
                  icon={"fas fa-key"}
                  value={localData?.password || ""}
                  change={dataChanged}
                  required={true}
                ></SimpleTextInputGroup>
              </div>
            </div>
          </form>
        </Modal.Body>
        <Modal.Footer
          className="border-top-0 mt-3 mb-3"
          style={{ paddingLeft: "3.5rem", paddingRight: "3.5rem" }}
        >
          <button
            type="submit"
            className="w-100 rounded-pill mb-4 btn btn-custom btn-primary-custom btn-smol"
            onClick={handleSubmit}
            ref={submitbtn}
          >
            Login
          </button>
        </Modal.Footer>
      </Modal>
    </>
  );
}
