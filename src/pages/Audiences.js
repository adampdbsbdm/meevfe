import React, { Fragment, useState, useEffect } from "react";
import SectionBar from "../components/SectionBar";
import { useHistory } from "react-router-dom";
import UserAudience from "./UserAudience";
import GroupAudience from "./GroupAudience";
import ExternalAudience from "./ExternalAudience";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";

export default function Audiences() {
  let history = useHistory();

  useEffect(() => {
    // API Fetch
  }, []);

  useEffect(() => {
    console.log(JSON.stringify(localData));
  });

  const [localData, setLocalData] = useState({
    availableNames: {
      personOptions: [
        { value: "person 1", label: "Person 1" },
        { value: "person 2", label: "Person 2" },
      ],
      groupOptions: [{ value: "2", label: "Group", color: "#36B37E" }],
      externalOptions: [{ value: "5", label: "External", color: "#00B8D9" }],
    },
  });

  const [wasvalidated, setWasvalidated] = useState(false);

  return (
    <Fragment>
      <SectionBar
        title="Audiences"
        subtitle="Configure Audiences Setting"
      ></SectionBar>
      <div className="wrapper">
        <div className="base-content">
          <form
            className={`container-fluid px-4${
              wasvalidated ? "was-validated" : ""
            }`} style={{marginTop:50}}
            noValidate
          >
            <Tabs>
              <TabList>
                <Tab>User</Tab>
                <Tab>Group</Tab>
                <Tab>External</Tab>
              </TabList>

              <TabPanel>
                <UserAudience />
              </TabPanel>
              <TabPanel>
                <GroupAudience />
              </TabPanel>
              <TabPanel>
                <ExternalAudience />
              </TabPanel>
            </Tabs>

            {/* <div className="d-flex flex-row mr-sm-4 justify-content-end submit-area">
              <button
                className="btn btn-custom btn-secondary mr-3"
                onClick={(e) => {
                  e.preventDefault();
                  // history.push("/audiences");
                  // setRoleList(dummyRole);
                  // setGroupList(dummyGroup);
                  // setExternalList(dummyExternal);
                  window.location.reload();
                }}
              >
                Cancel
              </button>
              <button
                className="btn btn-custom btn-primary-custom"
                type="submit"
                onClick={(e) => {
                  e.preventDefault();
                  setWasvalidated(true);
                }}
              >
                Save
              </button>
            </div> */}
          </form>
        </div>
      </div>
      <span
        style={{
          fontSize: "0.75em",
          display: "block",
          textAlign: "right",
          marginRight: "10px",
          marginBottom: "5px",
        }}
      >
        Meev@2020
      </span>
    </Fragment>
  );
}
