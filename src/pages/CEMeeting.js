import React, {
  Fragment,
  useState,
  useEffect,
  useRef,
  useContext,
} from "react";
import styles from "./CEMeeting.module.scss";
import SectionBar from "../components/SectionBar";
import SimpleTextInput from "../components/SimpleTextInput";
import AreaTextInput from "../components/AreaTextInput";
import LocationTextInput from "../components/LocationTextInput";
import PICLine from "../components/PICLine";
import ChipSelectInput from "../components/ChipSelectInput";
import SimpleDatetimePicker from "../components/SimpleDatetimePicker";
import { useHistory, useParams } from "react-router-dom";
import SimpleDragDropFile from "../components/SimpleDragDropFile";
import MultiTextOutput from "../components/MultiTextOutput";
import { getAllExternal, getAllGroup, getAllUser } from "../api/audience";
import { AUDCOLOR, PERSON } from "../types/ColorStatus";
import { createMeeting, editMeeting, getMeetingDetails } from "../api/meeting";
import { getKeyByValue } from "../helper/object";
import { UserContext } from "../data/user";
import * as MeetingStatusType from "../types/MeetingStatus";
import SingleDataSelectInput from "../components/SingleDataSelectInput";
import { toast } from "react-toastify";
import { failConfig, successConfig } from "../helper/toastConfig";
import client from "../api/request";

export default function CEMeeting(props) {
  let { datestart, id } = useParams();
  let history = useHistory();
  let mainform = useRef(null);
  // const userData = useContext(UserContext);

  useEffect(() => {
    const path = props.location.pathname.split("/")[2];
    if (path === "edit") {
      setEditMode(true);
      return populateEdit(id);
    } else {
      return populateCreateNew(datestart);
    }
  }, []);

  const populateCreateNew = (datestart) => {
    // https://devtrium.com/posts/async-functions-useeffect
    let isSubscribed = true;
    setSubtitle("Create New Meeting");

    const fetchData = async () => {
      try {
        const [user, group, external] = await Promise.all([
          getAllUser(),
          getAllGroup(),
          getAllExternal(),
        ]);

        if (isSubscribed) {
          const optionsMapping = (e, kind) => ({
            value: e._id,
            label: e.name,
            color: AUDCOLOR[kind],
          });
          let initialLocalData = {
            availableNames: {
              personOptions:
                user?.data?.map((e) => optionsMapping(e, "person")) || [],
              groupOptions: group?.data?.map((e) => optionsMapping(e, "group")) || [],
              externalOptions:
                external?.data?.map((e) => optionsMapping(e, "external")) || [],
            },
            availableLeaderNames:
              user?.data?.map((e) => optionsMapping(e, "person")) || [],
            candidateNames: [],
            enable_offline: false,
            enable_online: true,
            status: MeetingStatusType.NYS,
          };

          if (datestart) {
            let dateend = new Date(datestart);
            dateend.setTime(dateend.getTime() + 1000 * 60 * 60 * 1.5);
            dateend = dateend.toLocaleString();
            initialLocalData = { ...initialLocalData, datestart, dateend };

            // search conflict meeting
          }
          setLocalData(initialLocalData);
        }
      } catch (ex) {
        console.log(ex);
      }
    };

    fetchData().catch(console.error);

    // cancel any future `setData`
    return () => (isSubscribed = false);
  };

  const populateEdit = (id) => {
    // https://devtrium.com/posts/async-functions-useeffect
    let isSubscribed = true;
    setSubtitle("Edit Existing Meeting");

    const fetchData = async () => {
      try {
        const [user, group, external, meeting] = await Promise.all([
          getAllUser(),
          getAllGroup(),
          getAllExternal(),
          getMeetingDetails(id),
        ]);

        if (isSubscribed) {
          const optionsMapping = (e, kind) => ({
            value: e._id,
            label: e.name,
            color: AUDCOLOR[kind],
          });
          let initialLocalData = {
            ...meeting,
            leader: {
              value: meeting?.leader?.id,
              label: meeting?.leader?.name,
              color: AUDCOLOR[PERSON],
            },
            candidateNames:
              meeting?.attendees?.map((a) => {
                return { id: a.aid, name: a.name, color: AUDCOLOR[a.kind] };
              }) || [],
            availableNames: {
              personOptions:
                user?.data?.map((e) => optionsMapping(e, "person")) || [],
              groupOptions: group?.data?.map((e) => optionsMapping(e, "group")) || [],
              externalOptions:
                external?.data?.map((e) => optionsMapping(e, "external")) || [],
            },
            availableLeaderNames:
              user?.data?.map((e) => optionsMapping(e, "person")) || [],
            location_offline: meeting.location_offline,
            location_online: meeting.location_online,
            enable_offline: meeting.location_offline ? true : false,
            enable_online:
              meeting.location_online || !meeting.location_offline
                ? true
                : false,
          };

          const pics =
            meeting?.pic?.map((a) => {
              return { ...a, id: a.aid, name: { id: a.aid, name: a.name } };
            }) || [];

          setLocalData(initialLocalData);
          setPicList(pics);
        }
      } catch (ex) {
        console.log(ex);
      }
    };

    fetchData().catch(console.error);

    // cancel any future `setData`
    return () => (isSubscribed = false);
  };

  function dataChanged(o) {
    setLocalData({ ...localData, [o.key]: o.value });
  }

  function memberChanged(e) {
    const newline = {
      ...localData,
      [e.key]: { value: e.value, label: e.label },
    };
    setLocalData(newline);
  }

  function dateMeetingChanged(o) {
    const newLocalData = { ...localData, [o.key]: o.value };
    setLocalData(newLocalData);

    if (newLocalData.datestart !== null && newLocalData.dateend !== null) {
      // check
    }
  }

  function picLineChanged(line) {
    const lines = picList.map((l) => {
      if (l.id === line.id) {
        return { ...line };
      } else {
        return l;
      }
    });
    // const lines = [...picList.filter((l) => +l.id !== +line.id), line];
    setPicList(lines);
  }

  function picLineDeleted(line) {
    const lines = picList.filter((l) => l.id !== line.id);
    setPicList(lines);
  }

  function picLineAdded() {
    setPicList([
      ...picList,
      {
        id: Math.floor(Math.random(0, 1) * 10000).toString(),
        name: {
          id: "",
          name: "",
        },
        task: "",
        due: localData?.datestart || new Date()
      },
    ]);
  }

  function audiencesChanged(e) {
    setLocalData({ ...localData, candidateNames: e });
    setAvailableName(e);
  }

  function getSubmitData() {
    let data = {
      ...localData,
      location_offline: localData.enable_offline
        ? localData.location_offline
        : "",
      location_online: localData.enable_online ? localData.location_online : "",
      leader: {
        id: localData?.leader?.value || "",
        name: localData?.leader?.label || "",
      },
      attendees:
        localData?.candidateNames?.map(({ id, name, color }) => ({
          aid: id,
          name,
          kind: getKeyByValue(AUDCOLOR, color),
        })) || [],
      pic:
        picList?.map((pic) => ({
          ...pic,
          aid: pic.name.id,
          name: pic.name.name
        })) || [],
    };

    // console.log(localData);
    // console.log(data);

    return data;
  }

  const handleSubmit = async (e) => {
    e.preventDefault();
    setWasvalidated(true);
    // getSubmitData();
    if (
      mainform.current.checkValidity() &&
      (localData.location_online || localData.location_offline)
    ) {
      try {
        if (!editMode) {
          await createMeeting(getSubmitData());
          toast.success("Meeting created successfully", successConfig);
        } else {
          await editMeeting(getSubmitData(), localData._id);
          toast.success("Meeting updated successfully", successConfig);
        }
        history.push("/meeting");
      } catch(error) {
        const msg = error?.response?.data?.msg
        if(msg) {
          toast.error(msg, failConfig);
        } else {
          toast.error("Meeting cannot be submitted", failConfig);
        }
      }
    } else {
      toast.error("Meeting data invalid", failConfig);
    }
  };

  const [localData, setLocalData] = useState();

  const [picList, setPicList] = useState([]);

  const [availableName, setAvailableName] = useState([]);

  const [subtitle, setSubtitle] = useState();

  const [wasvalidated, setWasvalidated] = useState(false);

  const [conflictsMeetings, setConflictMeetings] = useState([]);

  const [editMode, setEditMode] = useState(false);

  return (
    <Fragment>
      <SectionBar title="Meeting" subtitle={subtitle}></SectionBar>
      <div className="wrapper">
        <div className="base-content">
          <form
            ref={mainform}
            className={`container-fluid px-4 ${
              wasvalidated ? "was-validated" : ""
            }`}
            onSubmit={(e) => {
              e.preventDefault();
            }}
            noValidate
          >
            <h4 className="mt-4 grouptext ml-sm-4">General Information</h4>
            <div className="d-flex flex-column ml-sm-4">
              <div className="row col-lg-12 col-xl-8 mt-4 px-0">
                <SimpleTextInput
                  name="event"
                  caption="Event"
                  placeholder=""
                  value={localData?.event || ""}
                  change={dataChanged}
                  required={true}
                ></SimpleTextInput>
              </div>
              <div className="row col-lg-12 col-xl-8 mt-4 px-0">
                {/* <SimpleTextInput
                  name="leader"
                  caption="Leader"
                  placeholder=""
                  value={localData?.leader || ""}
                  change={dataChanged}
                  required={true}
                ></SimpleTextInput> */}
                <label
                  className="col-sm-12 col-md-2"
                  style={{ marginTop: "0.5em" }}
                  htmlFor="leader"
                >
                  Leader
                </label>
                <div className="col-sm-12 col-md-10">
                  <SingleDataSelectInput
                    name="leader"
                    placeholder="Select leader name..."
                    value={localData?.leader || ""}
                    change={memberChanged}
                    availableOptions={localData?.availableLeaderNames?.map(
                      (o, i) => {
                        return { value: o.value, label: o.label };
                      }
                    )}
                    creatable={true}
                  ></SingleDataSelectInput>
                </div>
              </div>
              <div className="row col-lg-12 col-xl-8 mt-4 px-0">
                <LocationTextInput
                  offlineloc={localData?.location_offline || ""}
                  onlineloc={localData?.location_online || ""}
                  change={dataChanged}
                ></LocationTextInput>
              </div>
              <div className="row col-lg-12 col-xl-8 mt-4 px-0">
                <SimpleDatetimePicker
                  name="datestart"
                  caption="Start"
                  placeholder="Select start datetime"
                  initval={datestart}
                  value={localData?.datestart || ""}
                  change={dateMeetingChanged}
                  required={true}
                ></SimpleDatetimePicker>
              </div>
              <div className="row col-lg-12 col-xl-8 mt-4 px-0">
                <SimpleDatetimePicker
                  name="dateend"
                  caption="End"
                  placeholder="Select end datetime"
                  initval={datestart}
                  value={localData?.dateend || ""}
                  change={dateMeetingChanged}
                  required={true}
                ></SimpleDatetimePicker>
              </div>
              {conflictsMeetings.length > 0 && (
                <div className="row col-lg-12 col-xl-8 mt-4 px-0">
                  <MultiTextOutput
                    isError={true}
                    caption="Possible Conflicts"
                    value={conflictsMeetings?.map((a) => {
                      const s = new Date(a.datestart);
                      const e = new Date(a.dateend);
                      return `${a.event} - 
                    ${s.toLocaleDateString(
                      "en-US"
                    )} ${s.getHours()}:${s.getMinutes()}-${e.getHours()}:${e.getMinutes()}`;
                    })}
                  ></MultiTextOutput>
                </div>
              )}
              <div className="row col-lg-12 col-xl-8 mt-4 px-0">
                <AreaTextInput
                  name="agenda"
                  caption="Agenda"
                  placeholder=""
                  value={localData?.agenda}
                  change={dataChanged}
                  rows="5"
                  required={true}
                ></AreaTextInput>
              </div>
              <div className="row col-lg-12 col-xl-8 mt-4 px-0">
                <ChipSelectInput
                  name="attendees"
                  caption="Invitees"
                  placeholder="Invite audiences..."
                  value={localData?.candidateNames}
                  change={audiencesChanged}
                  availableNames={localData?.availableNames}
                ></ChipSelectInput>
              </div>
            </div>

            <h4 className="mt-4 grouptext ml-sm-4">PIC List</h4>
            <div className="d-flex flex-column mx-sm-4 mb-4 meev-input-list">
              <div className="col-12 px-0 header-list">
                <div className="row mx-0 col-11">
                  <div className="col-4 column-list">Name</div>
                  <div className="col-4 column-list">Task</div>
                  <div className="col-4 column-list">Due</div>
                </div>
              </div>

              {picList?.map((object, i) => (
                <PICLine
                  id={object.id}
                  name={{
                    value: object.name.id,
                    label: object.name.name,
                  }}
                  task={object.task}
                  due={object.due}
                  fullline={object}
                  key={object.id}
                  onChange={picLineChanged}
                  onDelete={picLineDeleted}
                  availableName={localData?.candidateNames?.map((o, i) => {
                    return { value: o.id, label: o.name };
                  })}
                />
              ))}
              <button
                type="button"
                className="col-12 px-0 blank-button text-primary d-flex align-items-center mt-4"
                onClick={picLineAdded}
              >
                <i className="fas fa-plus" style={{ fontSize: "1em" }}></i>
                <span className="add" style={{ marginLeft: "5px" }}>
                  Add new data
                </span>
              </button>
            </div>

            <h4 className="mt-4 grouptext ml-sm-4">Attachment</h4>
            <div className="d-flex flex-column ml-sm-4">
              <div className="row col-lg-12 col-xl-8 mt-4 px-0 mx-0">
                <SimpleDragDropFile
                  value={localData?.attachment}
                  change={dataChanged}
                  baseurl={client.defaults.baseURL}
                ></SimpleDragDropFile>
              </div>
            </div>

            <div className="d-flex flex-row mr-sm-4 justify-content-end submit-area">
              <button
                type="button"
                className="btn btn-custom btn-secondary mr-3"
                onClick={(e) => {
                  e.preventDefault();
                  history.push("/meeting");
                }}
              >
                Cancel
              </button>
              <button
                className="btn btn-custom btn-primary-custom"
                type="button"
                onClick={handleSubmit}
              >
                Submit
              </button>
            </div>
          </form>
        </div>
      </div>
      <span
        style={{
          fontSize: "0.75em",
          display: "block",
          textAlign: "right",
          marginRight: "10px",
          marginBottom: "5px",
        }}
      >
        Meev@2020
      </span>
    </Fragment>
  );
}

// useEffect(() => {
//   // API Fetch
//   // ...

//   //Edit
//   const path = props.location.pathname.split("/")[2];
//   if (path === "edit") {
//     setSubtitle("Edit Existing Meeting");
//     setLocalData({
//       event: "Regulasi data",
//       leader: "Budi",
//       agenda: "Pembahasan regulasi",
//       offlineloc: "Gedung A",
//       onlineloc: "https://meeet.com/ava2as21kfa",
//       attachment: "https://www.7-zip.org/a/7za920.zip",
//       start: "Fri Dec 10 2020 15:43:46 GMT+0700 (Indochina Time)",
//       end: "Fri Dec 10 2020 15:43:46 GMT+0700 (Indochina Time)",
//       availableNames: {
//         personOptions: [
//           { value: "person 1", label: "Person 1", color: "#4B4B4D" },
//           { value: "person 2", label: "Person 2", color: "#4B4B4D" },
//           { value: "person 3", label: "Person 3", color: "#4B4B4D" },
//         ],
//         groupOptions: [{ value: "2", label: "Group", color: "#36B37E" }],
//         externalOptions: [
//           { value: "5", label: "External", color: "#00B8D9" },
//         ],
//       },
//       candidateNames: [
//         { id: "person 1", name: "Person 1", color: "#4B4B4D" },
//         { id: "person 2", name: "Person 2", color: "#4B4B4D" },
//         { id: "person 3", name: "Person 3", color: "#4B4B4D" },
//         { id: "2", name: "Group", color: "#36B37E" },
//         { id: "5", name: "External", color: "#00B8D9" },
//       ],
//     });

//     setPicList([
//       {
//         id: "1",
//         name: {
//           id: "person 1",
//           name: "Person 1",
//         },
//         task: "task 1",
//         description: "detail task 1",
//       },
//       {
//         id: "2",
//         name: {
//           id: "person 2",
//           name: "Person 2",
//         },
//         task: "task 2",
//         description: "detail task 2",
//       },
//       {
//         id: "3",
//         name: {
//           id: "person 3",
//           name: "Person 3",
//         },
//         task: "task 2",
//         description: "detail task 3",
//       },
//     ]);
//   } else {
//     setSubtitle("Create New Meeting");
//     let initialLocalData = {
//       availableNames: {
//         personOptions: [
//           { value: "person 1", label: "Person 1", color: "#4B4B4D" },
//           { value: "person 2", label: "Person 2", color: "#4B4B4D" },
//         ],
//         groupOptions: [{ value: "2", label: "Group", color: "#36B37E" }],
//         externalOptions: [
//           { value: "5", label: "External", color: "#00B8D9" },
//         ],
//       },
//       candidateNames: [],
//     }
//     if (start) {
//       let end = new Date(start);
//       end.setTime(end.getTime() + 1000 * 60 * 60 * 1.5);
//       end = end.toLocaleString()
//       initialLocalData = { ...initialLocalData, start, end };
//     }

//     setLocalData(initialLocalData);

//     // setConflictMeetings([
//     //   {
//     //     event: "Regulasi data",
//     //     start: "Fri Dec 10 2020 15:43:46 GMT+0700 (Indochina Time)",
//     //     end: "Fri Dec 10 2020 15:43:46 GMT+0700 (Indochina Time)",
//     //   },
//     //   {
//     //     event: "Regulasi data 2",
//     //     start: "Fri Dec 10 2020 15:43:46 GMT+0700 (Indochina Time)",
//     //     end: "Fri Dec 10 2020 15:43:46 GMT+0700 (Indochina Time)",
//     //   },
//     // ]);
//   }
// }, []);
