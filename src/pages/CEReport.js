import React, { Fragment, useState, useEffect, useRef } from "react";
import styles from "./CEMeeting.module.scss";
import SectionBar from "../components/SectionBar";
import * as MeetingStatusType from "../types/MeetingStatus";
import NoteLine from "../components/NoteLine";
import DecisionLine from "../components/DecisionLine";
import ChipSelectInput from "../components/ChipSelectInput";
import SingleDataSelectInput from "../components/SingleDataSelectInput";
import { editReport, getReportDetails } from "../api/report";
import { AUDCOLOR } from "../types/ColorStatus";
import { getAllExternal, getAllGroup, getAllUser } from "../api/audience";
import { useParams, useHistory } from "react-router-dom";
import { getKeyByValue } from "../helper/object";
import SimpleDragDropFile from "../components/SimpleDragDropFile";
import { toast } from "react-toastify";
import { failConfig, successConfig } from "../helper/toastConfig";
import client from "../api/request";

export default function CEReport() {
  let { id } = useParams();
  let history = useHistory();
  let mainform = useRef(null);

  useEffect(() => {
     return populateEdit(id);
  }, []);

  const populateEdit = (id) => {
    // https://devtrium.com/posts/async-functions-useeffect
    let isSubscribed = true;

    const fetchData = async () => {
      try {
        const [user, group, external, meeting] = await Promise.all([
          getAllUser(),
          getAllGroup(),
          getAllExternal(),
          getReportDetails(id),
        ]);

        if (isSubscribed) {
          const optionsMapping = (e, kind) => ({
            value: e._id,
            label: e.name,
            color: AUDCOLOR[kind],
          });
          let initialLocalData = {
            ...meeting,
            status: { value: 1, label: MeetingStatusType.DONE },
            attachment: meeting?.report?.attachment,
            attendees: meeting?.report?.attendees?.map((a) => {
              return { id: a.aid, name: a.name, color: AUDCOLOR[a.kind] };
            }) || [],
            availableNames: {
              personOptions:
                user?.data?.map((e) => optionsMapping(e, "person")) || [],
              groupOptions: group?.data?.map((e) => optionsMapping(e, "group")) || [],
              externalOptions:
                external?.data?.map((e) => optionsMapping(e, "external")) || [],
            },
            
          };
          const notes = meeting?.report?.notes?.map((a) => {
            return { ...a, id: a._id };
          }) || [];

          const decisions = meeting?.report?.pic?.map((a) => {
            return { ...a, id: a._id, pic: { id: a.aid, name: a.name } };
          }) || [];

          setLocalData(initialLocalData);
          setNoteList(notes)
          setDecisionList(decisions);
        }
      } catch (ex) {
        console.log(ex);
      }
    };

    fetchData().catch(console.error);

    // cancel any future `setData`
    return () => (isSubscribed = false);
  };

  function dataChanged(o) {
    setLocalData({ ...localData, [o.key]: o.value });
  }

  function memberChanged(e) {
    const newline = { ...localData, [e.key]: { value: e.value, label: e.label } };
    setLocalData(newline);
  }

  function noteLineChanged(line) {
    const lines = noteList.map((l) => {
      console.log(line);
      if (l.id === line.id) {
        return { ...line };
      } else {
        return l;
      }
    });
    // const lines = [...noteList.filter((l) => +l.id !== +line.id), line];
    setNoteList(lines);
  }

  function noteLineDeleted(line) {
    const lines = noteList.filter((l) => l.id !== line.id);
    setNoteList(lines);
  }

  function noteLineAdded() {
    setNoteList([
      ...noteList,
      {
        id: Math.floor(Math.random(0, 1) * 10000).toString(),
        point: "",
        notes: "",
      },
    ]);
  }

  function decisionLineChanged(line) {
    const lines = decisionList.map((l) => {
      if (l.id === line.id) {
        return { ...line };
      } else {
        return l;
      }
    });
    // const lines = [...decisionList.filter((l) => +l.id !== +line.id), line];
    setDecisionList(lines);
  }

  function decisionLineDeleted(id) {
    const lines = decisionList.filter((l) => l.id !== id);
    setDecisionList(lines);
  }

  function decisionLineAdded() {
    setDecisionList([
      ...decisionList,
      {
        id: Math.floor(Math.random(0, 1) * 10000).toString(),
        decision: "",
        pic: {
          id: "",
          name: "",
        },
        detail: "",
      },
    ]);
  }

  function audiencesChanged(e) {
    console.log(e);
    setLocalData({ ...localData, attendees: e });
  }

  useEffect(() => {
    // console.log(JSON.stringify(localData));
  });

  
  function getSubmitData() {
    let data = {
      status: localData?.status?.label,
      attachment: localData?.attachment,
      attendees:
        localData?.attendees?.map(({ id, name, color }) => ({
          aid: id,
          name,
          kind: getKeyByValue(AUDCOLOR, color),
        })) || [],
      notes:
        noteList?.map(({ point, notes }) => ({
          notes,
          point,
        })) || [],
      pic:
        decisionList?.map(({ pic, decision, detail, due }) => ({
          aid: pic.id,
          name: pic.name,
          decision,
          detail,
          due,
        })) || [],
    };

    console.log(data);
    return data;
  }

  const handleSubmit = async (e) => {
    e.preventDefault();
    setWasvalidated(true);
    getSubmitData();
    if (mainform.current.checkValidity()) {
      try {
        await editReport(getSubmitData(), localData.id);
        toast.success("Report submitted successfully", successConfig);
        history.push("/meeting");
      } catch(error) {
        const msg = error?.response?.data?.msg
        if(msg) {
          toast.error(msg, failConfig);
        } else {
          toast.error("Meeting cannot be submitted", failConfig);
        }
      }
    } else {
      toast.error("Report data invalid", failConfig);
    }
  };

  const [localData, setLocalData] = useState();

  const [noteList, setNoteList] = useState([]);

  const [decisionList, setDecisionList] = useState([]);

  const [wasvalidated, setWasvalidated] = useState(false);

  return (
    <Fragment>
      <SectionBar title="Meeting" subtitle="Submit Report"></SectionBar>
      <div className="wrapper">
        <div className="base-content">
          <form
            ref={mainform}
            className={`container-fluid px-4 ${
              wasvalidated ? "was-validated" : ""
            }`}
            onSubmit={(e) => {
              e.preventDefault();
            }}
            noValidate
          >
            <h4 className="mt-4 grouptext ml-sm-4">General Information</h4>
            <div className="d-flex flex-column ml-sm-4">
              <div className="row col-lg-12 col-xl-8 mt-4 px-0">
                <SingleDataSelectInput
                  name="status"
                  caption="Status"
                  placeholder="Select meeting status..."
                  value={localData?.status}
                  change={memberChanged}
                  required={true}
                  availableOptions={MeetingStatusType.MEETING_STATUS_LIST.map(
                    (o, i) => {
                      return { value: i + 1, label: o };
                    }
                  )}
                ></SingleDataSelectInput>
              </div>
              <div className="row col-lg-12 col-xl-8 mt-4 px-0">
                <ChipSelectInput
                  name="attendees"
                  caption="Attendees"
                  placeholder="attendee list..."
                  value={localData?.attendees}
                  change={audiencesChanged}
                  availableNames={localData?.availableNames}
                ></ChipSelectInput>
              </div>
            </div>

            <h4 className="mt-4 grouptext ml-sm-4">Note List</h4>
            <div className="d-flex flex-column mx-sm-4 mb-4 meev-input-list">
              <div className="col-12 px-0 header-list">
                <div className="row mx-0 col-11">
                  <div className="col-4 column-list">Point / Case</div>
                  <div className="col-8 column-list">Notes</div>
                </div>
              </div>

              {noteList?.map((object, i) => (
                <NoteLine
                  id={object.id}
                  point={object.point}
                  notes={object.notes}
                  key={object.id}
                  onChange={noteLineChanged}
                  onDelete={noteLineDeleted}
                />
              ))}
              <button
                type="button"
                className="col-12 px-0 blank-button text-primary d-flex align-items-center mt-4"
                onClick={noteLineAdded}
              >
                <i className="fas fa-plus" style={{ fontSize: "1em" }}></i>
                <span className="add" style={{ marginLeft: "5px" }}>
                  Add new data
                </span>
              </button>
            </div>

            <h4 className="mt-4 grouptext ml-sm-4">Decision List</h4>
            <div className="d-flex flex-column mx-sm-4 mb-4 meev-input-list">
              <div className="col-12 px-0 header-list">
                <div className="row mx-0 col-11">
                  <div className="col-3 column-list">PIC</div>
                  <div className="col-3 column-list">Decision</div>
                  <div className="col-3 column-list">Detail</div>
                  <div className="col-3 column-list">Due</div>
                </div>
              </div>

              {decisionList?.map((object, i) => (
                <DecisionLine
                  id={object.id}
                  pic={{ value: object.pic.id, label: object.pic.name }}
                  decision={object.decision}
                  detail={object.detail}
                  due={object.due}
                  key={object.id}
                  onChange={decisionLineChanged}
                  onDelete={decisionLineDeleted}
                  availableName={localData?.attendees?.map((o, i) => {
                    return { value: o.id, label: o.name };
                  })}
                />
              ))}
              <button
                type="button"
                className="col-12 px-0 blank-button text-primary d-flex align-items-center mt-4"
                onClick={decisionLineAdded}
              >
                <i className="fas fa-plus" style={{ fontSize: "1em" }}></i>
                <span className="add" style={{ marginLeft: "5px" }}>
                  Add new data
                </span>
              </button>
            </div>

            <h4 className="mt-4 grouptext ml-sm-4">Attachment</h4>
            <div className="d-flex flex-column ml-sm-4">
              <div className="row col-lg-12 col-xl-8 mt-4 px-0 mx-0">
                <SimpleDragDropFile
                  value={localData?.attachment}
                  change={dataChanged}
                  baseurl={client.defaults.baseURL}
                ></SimpleDragDropFile>
              </div>
            </div>

            <div className="d-flex flex-row mr-sm-4 justify-content-end submit-area">
              <button
                className="btn btn-custom btn-secondary mr-3"
                type="button"
                onClick={(e) => {
                  e.preventDefault();
                  history.push("/meeting");
                }}
              >
                Cancel
              </button>
              <button
                className="btn btn-custom btn-primary-custom"
                type="button"
                onClick={handleSubmit}
              >
                Submit
              </button>
            </div>
          </form>
        </div>
      </div>
      <span
        style={{
          fontSize: "0.75em",
          display: "block",
          textAlign: "right",
          marginRight: "10px",
          marginBottom: "5px",
        }}
      >
        Meev@2020
      </span>
    </Fragment>
  );
}

// useEffect(() => {
//   // API Fetch
//   // ...

//   setLocalData({
//     status: { value: 1, label: MeetingStatusType.DONE },
//     availableNames: {
//       personOptions: [
//         { value: "person 1", label: "Person 1", color: "#4B4B4D" },
//         { value: "person 2", label: "Person 2", color: "#4B4B4D" },
//       ],
//       groupOptions: [{ value: "2", label: "Group", color: "#36B37E" }],
//       externalOptions: [{ value: "5", label: "External", color: "#00B8D9" }],
//     },
//     attendees: [
//       { id: "person 1", name: "Person 1", color: "#4B4B4D" },
//       { id: "2", name: "Group", color: "#36B37E" },
//       { id: "5", name: "External", color: "#00B8D9" },
//     ],
//   });

//   setDecisionList([
//     {
//       id: "1",
//       decision: "decision 1",
//       pic: {
//         id: "person 1",
//         name: "Person 1",
//       },
//       detail: "detail 1",
//       due: "Tue Dec 16 2020 12:53:44 GMT+0700 (Indochina Time)",
//     },
//     {
//       id: "2",
//       decision: "decision 2",
//       pic: {
//         id: "person 2",
//         name: "Person 2",
//       },
//       detail: "detail 2",
//       due: "Tue Dec 29 2020 12:53:44 GMT+0700 (Indochina Time)",
//     },
//   ]);

//   setNoteList([
//     {
//       id: "1",
//       point: "point 1",
//       notes: "notes 1",
//     },
//     {
//       id: "2",
//       point: "point 2",
//       notes: "notes 2",
//     },
//   ]);
// }, []);