import React, { Fragment, useState, useEffect, useRef } from "react";
import RoleLine from "../components/RoleLine";
import ReactPaginate from "react-paginate";
import SearchBar from "../components/SearchBar";
import { toast } from "react-toastify";
import { failConfig, successConfig } from "../helper/toastConfig";
import * as RoleStatusType from "../types/RoleStatus";
import LoadingScreen from "../components/Loading";
import {
  changeUserPassword,
  createUser,
  deleteUser,
  editUser,
  getAllUser,
} from "../api/audience";
import swal, { getConfirmConfig, getInputConfig } from "../components/Alert";
import ShowDetails from "./ShowDetails";
import NoData from "../components/NoData";

export default function UserAudience() {
  const showDetailsRef = useRef();
  const [currentPage, setCurrentPage] = useState(0);
  const [itemOffset, setItemOffset] = useState(0);
  const [pageCount, setPageCount] = useState(0);
  const [itemsPerPage, setItemPerPage] = useState(5);
  const [roleList, setRoleList] = useState([]);
  const [totalPage, setTotalPage] = useState(0);
  const [keyword, setKeyword] = useState("");
  const [onSearch, setOnSearch] = useState(false);
  const [loading, setLoading] = useState(true);
  const [updateCount, setUpdateCount] = useState(0);
  const [selectedLine, setSelectedLine] = useState();
  const [modalShowDetails, setModalShowDetails] = useState(false);

  useEffect(() => {
    return populateData(itemsPerPage, itemOffset);
  }, [itemOffset, itemsPerPage, onSearch]);

  const populateData = (itemsPerPage, itemOffset) => {
    let isSubscribed = true;
    setLoading(true);

    const fetchData = async () => {
      const user = await getAllUser(itemsPerPage, itemOffset, keyword);
      if (isSubscribed) {
        const usr = user?.data?.map((d) => {
          return {
            ...d,
            id: d._id,
            role: { value: d.privilege, label: d.privilege },
          };
        });

        const npage = Math.ceil(user?.total / itemsPerPage);
        setRoleList([...usr]);
        setPageCount(npage);
        setTotalPage(user?.total);
        setOnSearch(false);
        handleInvalidPage(npage);

        setLoading(false);
      }
    };

    fetchData().catch(console.error);

    // cancel any future `setData`
    return () => (isSubscribed = false);
  };

  async function refreshPage() {
    if (itemOffset === 0) {
      await populateData(itemsPerPage, itemOffset);
    } else {
      setItemOffset(0);
    }
    setCurrentPage(0);
  }

  function roleLineChanged(line) {
    const lines = roleList.map((l) => {
      if (l.id === line.id) {
        return { ...line };
      } else {
        return l;
      }
    });
    setRoleList(lines);
  }

  async function roleLineDeleted(id) {
    try {
      await deleteUser(id);
      await populateData(itemsPerPage, itemOffset);
      toast.success("User audiences deleted successfully", successConfig);
    } catch (error) {
      const msg = error?.response?.data?.msg;
      if (msg) {
        toast.error(msg, failConfig);
      } else {
        toast.error("User audiences cannot be deleted", failConfig);
      }
    }
  }

  async function roleLineUpdated(id) {
    try {
      const line = roleList.filter((l) => l.id === id)[0];
      console.log(line);
      await editUser(line, id);

      toast.success("User audiences updated successfully", successConfig);
    } catch (error) {
      const msg = error?.response?.data?.msg;
      if (msg) {
        toast.error(msg, failConfig);
      } else {
        toast.error("User audiences cannot be updated", failConfig);
      }
    }
  }

  async function roleLineAdded() {
    try {
      // Ask Username
      const { value } = await swal.fire(
        getInputConfig(
          "Input Username",
          "Username for new user",
          "Enter unique username (min. 6 char)",
          "text"
        )
      );
      if (!value) {
        return;
      }

      if (itemOffset !== 0) {
        const result = await swal.fire(
          getConfirmConfig(
            "Adding User?",
            "You will be redirected to first page after adding data",
            "info"
          )
        );
        if (!result.isConfirmed) {
          return;
        }
      }

      const newUser = {
        id: "",
        username: value,
        name: value,
        privilege: RoleStatusType.MEMBER,
        role: { value: RoleStatusType.MEMBER, label: RoleStatusType.MEMBER },
        password: value,
      };

      const data = await createUser(newUser);
      // newUser.id = data.id;
      // setRoleList([newUser, ...roleList]);
      await refreshPage();
    } catch (error) {
      const msg = error?.response?.data?.msg;
      if (msg) {
        toast.error(msg, failConfig);
      } else {
        toast.error("User audiences cannot be created", failConfig);
      }
    }
  }

  async function roleLineChangePassword(id) {
    try {
      // Ask Username
      const { value } = await swal.fire(
        getInputConfig(
          "Change Password",
          "New password for given username",
          "Enter password (min. 6 char)",
          "password"
        )
      );
      if (!value) {
        return;
      }

      const newPassword = {
        password: value,
      };

      await changeUserPassword(newPassword, id);
      toast.success(
        "User audiences password updated successfully",
        successConfig
      );
    } catch (error) {
      const msg = error?.response?.data?.msg;
      if (msg) {
        toast.error(msg, failConfig);
      } else {
        toast.error("User audiences password cannot be changed", failConfig);
      }
    }
  }

  function roleLineShowDetails(id) {
    setSelectedLine(roleList.filter((l) => l.id === id)[0]);
    setModalShowDetails(true);
  }

  function roleLineModeChanged(inc) {
    setUpdateCount(updateCount + inc);
  }

  // Invoke when user click to request another page.
  const handlePageClick = (event) => {
    setCurrentPage(event.selected); // harus set manual, jk tdk maka ga bisa forcepage
    const newOffset = (event.selected * itemsPerPage) % totalPage;
    setItemOffset(newOffset);
  };

  const handleSearch = (keyword) => {
    setKeyword(keyword);
    setCurrentPage(0);
    setItemOffset(0);
    setOnSearch(true);
  };

  const handleInvalidPage = (npage) => {
    if (currentPage && currentPage > npage - 1) {
      const newCurrentPage = currentPage - 1;
      setCurrentPage(newCurrentPage);
      const newOffset = (newCurrentPage * itemsPerPage) % npage;
      setItemOffset(newOffset);
    }
  };

  return (
    <Fragment>
      <div className="container-fluid px-0">
        <div className="row mb-1 mx-sm-4 d-flex justify-content-between">
          <h4 className="mt-2 grouptext" style={{ marginLeft: 2 }}>
            Role Audiences
          </h4>
          <div
            className="col-sm-12 col-md-5 my-2 px-0 d-flex align-items-end 
          justify-content-md-end justify-content-sm-start"
          >
            <SearchBar
              setKeyword={handleSearch}
              placeholder="Find audience..."
            />
          </div>
        </div>
        <div className="d-flex flex-column mx-sm-4 mb-4 meev-input-list">
          <div className="col-12 px-0 header-list">
            <div className="row mx-0 col-10">
              <div className="col-4 column-list">Username</div>
              <div className="col-4 column-list">Name</div>
              <div className="col-4 column-list">Privilege</div>
            </div>
          </div>
          {roleList?.map((object, i) => (
            <RoleLine
              id={object.id}
              username={object.username}
              name={object.name}
              role={object.role}
              fullline={object}
              key={object.id}
              onChange={roleLineChanged}
              onDelete={roleLineDeleted}
              onUpdate={roleLineUpdated}
              editMode={object.isEdit}
              onEditModeChange={roleLineModeChanged}
              onChangePassword={roleLineChangePassword}
              onShowDetails={roleLineShowDetails}
            />
          ))}
          {!loading && (!roleList || roleList.length == 0) && <NoData className={"mt-4"} />}
          <button
            type="button"
            className="col-12 px-0 blank-button text-primary d-flex align-items-center mt-4"
            onClick={roleLineAdded}
          >
            <i className="fas fa-plus" style={{ fontSize: "1em" }}></i>
            <span className="add" style={{ marginLeft: "5px" }}>
              Add new data
            </span>
          </button>
        </div>
      </div>
      <ShowDetails
        isShow={modalShowDetails}
        username={selectedLine?.username}
        name={selectedLine?.name}
        contact={selectedLine?.contact}
        email={selectedLine?.email}
        privilege={selectedLine?.privilege}
        onClose={() => {
          setModalShowDetails(false);
        }}
      />
      <div className="d-flex justify-content-end mb-4 mx-sm-4 px-4">
        <ReactPaginate
          nextLabel="next >"
          onPageChange={handlePageClick}
          pageRangeDisplayed={3}
          marginPagesDisplayed={2}
          pageCount={pageCount}
          previousLabel="< previous"
          pageClassName="page-item"
          pageLinkClassName="page-link"
          previousClassName="page-item"
          previousLinkClassName="page-link"
          nextClassName="page-item"
          nextLinkClassName="page-link"
          breakLabel="..."
          breakClassName="page-item"
          breakLinkClassName="page-link"
          containerClassName="pagination"
          activeClassName="active"
          renderOnZeroPageCount={null}
          forcePage={currentPage}
        />
      </div>
    </Fragment>
  );
}
