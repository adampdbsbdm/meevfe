import React, { Fragment, useState, useEffect, useContext } from "react";
import { Link } from "react-router-dom";
import MeetingTableLine from "../components/MeetingTableLine";
import SectionBar from "../components/SectionBar";
import * as MeetingStatusType from "../types/MeetingStatus";
import { UserContext } from "../data/user";
import { deleteMeeting, getAllMeetings } from "../api/meeting";
import { MEMBER } from "../types/RoleStatus";
import ReactPaginate from "react-paginate";
import SearchBar from "../components/SearchBar";
import { toast } from "react-toastify";
import { failConfig, successConfig } from "../helper/toastConfig";
import LoadingScreen from "../components/Loading";
import NoData from "../components/NoData";
import client from "../api/request";

export default function Meeting() {
  const [currentPage, setCurrentPage] = useState();
  const [itemOffset, setItemOffset] = useState(0);
  const [pageCount, setPageCount] = useState(0);
  const [itemsPerPage, setItemPerPage] = useState(5);
  const [meetList, setMeetList] = useState([]);
  const [totalPage, setTotalPage] = useState(0);
  const [keyword, setKeyword] = useState("");
  const [onSearch, setOnSearch] = useState(false);
  const [loading, setLoading] = useState(true);
  const [color, setColor] = useState("#ffffff");

  const user = useContext(UserContext);

  useEffect(() => {
    // Fetch items from another resources.
    const endOffset = itemOffset + itemsPerPage;
    console.log(`Loading items from ${itemOffset} to ${endOffset}`);
    return populateData(itemsPerPage, itemOffset);
  }, [itemOffset, itemsPerPage, onSearch]);

  const populateData = (itemsPerPage, itemOffset) => {
    // https://devtrium.com/posts/async-functions-useeffect
    let isSubscribed = true;
    setLoading(true);

    const fetchData = async () => {
      const data = await getAllMeetings(itemsPerPage, itemOffset, keyword);
      if (isSubscribed) {
        const meet = data?.data?.map((d) => {
          return {
            ...d,
            id: d._id,
            status: d.status || MeetingStatusType.DONE,
          };
        });

        const npage = Math.ceil(data?.total / itemsPerPage);
        setMeetList([...meet]);
        setPageCount(npage);
        setTotalPage(data?.total);
        setOnSearch(false);
        handleInvalidPage(npage);

        setLoading(false);
      }
    };

    fetchData().catch(console.error);

    // cancel any future `setData`
    return () => (isSubscribed = false);
  };

  function meetListLineChanged(line) {
    // const lines = [...picList.filter((l) => +l.id !== +line.id), line];
    // setPicList(lines);
  }

  async function meetListLineDeleted(id) {
    try {
      await deleteMeeting(id);
      // const lines = meetList.filter((l) => l.id !== id);
      // setMeetList(lines);
      // const totalData = totalPage - 1
      // const npage = Math.ceil(totalData / itemsPerPage)
      await populateData(itemsPerPage, itemOffset);
      toast.success("Meeting deleted successfully", successConfig);
    } catch (error) {
      const msg = error?.response?.data?.msg;
      if (msg) {
        toast.error(msg, failConfig);
      } else {
        toast.error("Meeting cannot be deleted", failConfig);
      }
    }
  }

  // Invoke when user click to request another page.
  const handlePageClick = (event) => {
    setCurrentPage(event.selected); // harus set manual, jk tdk maka ga bisa forcepage
    const newOffset = (event.selected * itemsPerPage) % totalPage;
    console.log(
      `User requested page number ${event.selected}, which is offset ${newOffset}`
    );
    setItemOffset(newOffset);
  };

  const handleSearch = (keyword) => {
    setKeyword(keyword);
    setCurrentPage(0);
    setItemOffset(0);
    setOnSearch(true);
  };

  const handleInvalidPage = (npage) => {
    if (currentPage && currentPage > npage - 1) {
      const newCurrentPage = currentPage - 1;
      setCurrentPage(newCurrentPage);
      const newOffset = (newCurrentPage * itemsPerPage) % npage;
      setItemOffset(newOffset);
    }
  };

  // const [meetList, setMeetList] = useState([
  //   {
  //     id: "1",
  //     event: "Regulasi Frekuensi",
  //     datestart: "03-03-2020 10:30",
  //     dateend: "03-03-2020 11:30",
  //     pic: "Yes",
  //     attachment:
  //       "https://www.gravatar.com/avatar/f9f64a49c04ab42bd1c59c76b76ef45c?s=32&d=identicon&r=PG",
  //     status: MeetingStatusType.DONE,
  //   },
  //   {
  //     id: "2",
  //     event: "Regulasi Frekuensi",
  //     datestart: "03-03-2020 10:30",
  //     dateend: "03-03-2020 11:30",
  //     pic: "No",
  //     attachment: "",
  //     status: MeetingStatusType.NYS,
  //   },
  // ]);

  return (
    <Fragment>
      <SectionBar title="Meeting"></SectionBar>
      <div className="wrapper">
        <div className="base-content d-flex flex-column justify-content-between">
          {/* <div className="d-flex row justify-content-between"> */}
          <div className="container-fluid px-4">
            {/* <div className="row spacing-group mx-sm-4"> */}
            <div className="row spacing-group mb-1 mx-sm-4 d-flex justify-content-between">
              <h4 className="title-group col-sm-12 col-md-4 my-2 d-flex align-items-center">
                Meeting List
              </h4>
              {/* {user?.privilege?.toLowerCase() !== MEMBER.toLowerCase() && (
                  <div className="col-sm-12 col-md-8 my-2 d-flex align-items-center justify-content-md-end justify-content-sm-start">
                    <Link
                      to="/meeting/create"
                      className="px-0 blank-button text-primary d-flex align-items-center justify-content-end"
                    >
                      <i
                        className="fas fa-plus"
                        style={{ fontSize: "1em" }}
                      ></i>
                      <span className="add" style={{ marginLeft: "5px" }}>
                        Add new data
                      </span>
                    </Link>
                  </div>
                )} */}

              <div className="col-sm-12 col-md-5 my-2 d-flex align-items-center justify-content-md-end justify-content-sm-start">
                <SearchBar
                  setKeyword={handleSearch}
                  placeholder="Find meeting..."
                />
              </div>
            </div>
            {user?.privilege &&
              user?.privilege?.toLowerCase() !== MEMBER.toLowerCase() && (
                <div className="row mx-sm-4 mb-3 mt-3">
                  <div className="col-sm-12 col-md-12 d-flex align-items-center justify-content-md-end justify-content-sm-start">
                    <Link
                      to="/meeting/create"
                      className="px-0 blank-button text-primary d-flex align-items-center justify-content-end"
                    >
                      <i
                        className="fas fa-plus"
                        style={{ fontSize: "1em" }}
                      ></i>
                      <span className="add" style={{ marginLeft: "5px" }}>
                        Add new data
                      </span>
                    </Link>
                  </div>
                </div>
              )}
            <div className="d-flex flex-column mx-sm-4 mb-4 meev-input-list2">
              <div className="col-12 px-0 header-list">
                <div className="row mx-0 col-11">
                  <div className="col-3 column-list">EVENT</div>
                  {user ? (
                    <Fragment>
                      <div className="col-2 column-list">START</div>
                      <div className="col-2 column-list">END</div>
                      <div className="col-2 column-list">AS PIC</div>
                    </Fragment>
                  ) : (
                    <Fragment>
                      <div className="col-3 column-list">START</div>
                      <div className="col-3 column-list">END</div>
                    </Fragment>
                  )}
                  <div className="col-2 column-list">ATTACHMENT</div>
                  <div className="col-1 column-list">STATUS</div>
                </div>
              </div>
              {!loading &&
                meetList.map(
                  (
                    {
                      id,
                      event,
                      datestart,
                      dateend,
                      pic,
                      attachment,
                      status,
                      creator,
                    },
                    i
                  ) => (
                    <MeetingTableLine
                      id={id}
                      event={event}
                      datestart={datestart}
                      dateend={dateend}
                      pic={pic}
                      attachment={attachment}
                      status={status}
                      key={id}
                      onChange={meetListLineChanged}
                      onDelete={meetListLineDeleted}
                      creator={creator}
                      user={user}
                      baseurl={client.defaults.baseURL}
                    />
                  )
                )}
              {!loading && (!meetList || meetList.length == 0) && <NoData className={"mt-4"} />}
            </div>
          </div>
          <div className="d-flex justify-content-end mb-4 mx-sm-4 px-4">
            <ReactPaginate
              nextLabel="next >"
              onPageChange={handlePageClick}
              pageRangeDisplayed={3}
              marginPagesDisplayed={2}
              pageCount={pageCount}
              previousLabel="< previous"
              pageClassName="page-item"
              pageLinkClassName="page-link"
              previousClassName="page-item"
              previousLinkClassName="page-link"
              nextClassName="page-item"
              nextLinkClassName="page-link"
              breakLabel="..."
              breakClassName="page-item"
              breakLinkClassName="page-link"
              containerClassName="pagination"
              activeClassName="active"
              renderOnZeroPageCount={null}
              forcePage={currentPage}
            />
          </div>
          {/* </div> */}
        </div>
      </div>
      <LoadingScreen loading={loading} />
      <span
        style={{
          fontSize: "0.75em",
          display: "block",
          textAlign: "right",
          marginRight: "10px",
          marginBottom: "5px",
        }}
      >
        Meev@2020
      </span>
    </Fragment>
  );
}
