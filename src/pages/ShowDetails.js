import React, { useState, useEffect } from "react";
import Modal from "react-bootstrap/Modal";
import SimpleTextOutput from "../components/SimpleTextOutput";

export default function ShowDetails({
  isShow,
  username,
  name,
  contact,
  email,
  privilege,
  onClose,
}) {
  const [show, setShow] = useState(false);

  useEffect(() => {
    setShow(isShow);
  }, [isShow]);

  const handleClose = () => {
    onClose();
  };

  return (
    <>
      <Modal
        centered
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header
          className="justify-content-between 
          align-items-center border-bottom-0"
        >
          <h4 className="ml-4 my-2" style={{ fontSize: "1.2rem" }}>
            Account Details
          </h4>
          <button
            type="button"
            className="blank-button text-secondary d-flex align-items-center 
            justify-content-center col-1"
            onClick={handleClose}
          >
            <i className="fas fa-times" style={{ fontSize: "1.3em" }}></i>
          </button>
        </Modal.Header>
        <Modal.Body className="py-0 px-0 mb-4">
          <div className="wrapper">
            <div className="base-content w-100">
              <div className="container-fluid px-4">
                <div
                  className="d-flex flex-column"
                  style={{ paddingLeft: "2rem", paddingRight: "2rem" }}
                >
                  <div className="row col-lg-12 mt-2 px-0">
                    <SimpleTextOutput
                      caption="Username"
                      value={username}
                      spacing={[5, 7]}
                    ></SimpleTextOutput>
                  </div>
                  <div className="row col-lg-12 mt-3 px-0">
                    <SimpleTextOutput
                      caption="Name"
                      value={name}
                      spacing={[5, 7]}
                    ></SimpleTextOutput>
                  </div>
                  <div className="row col-lg-12 mt-3 px-0">
                    <SimpleTextOutput
                      caption="Contact"
                      value={contact}
                      spacing={[5, 7]}
                    ></SimpleTextOutput>
                  </div>
                  <div className="row col-lg-12 mt-3 px-0">
                    <SimpleTextOutput
                      caption="Email"
                      value={email}
                      spacing={[5, 7]}
                    ></SimpleTextOutput>
                  </div>
                  <div className="row col-lg-12 mt-3 px-0">
                    <SimpleTextOutput
                      caption="Privilege"
                      value={privilege}
                      spacing={[5, 7]}
                    ></SimpleTextOutput>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    </>
  );
}
