import { toast } from "react-toastify";

export const successConfig = {
  position: toast.POSITION.TOP_CENTER,
  theme: "light",
  toastId: "sukses",
  hideProgressBar: true,
};

export const failConfig = {
  position: toast.POSITION.TOP_CENTER,
  theme: "light",
  toastId: "fail",
  hideProgressBar: true,
};
