import client from "./request";

export const getAllTasks = async (limit, skip, keyword) => {
  try {
    let req;
    if (!limit) {
      req = await client.get(`api/task?s=${keyword}`);
    } else {
      req = await client.get(`api/task?limit=${limit}&skip=${skip}&s=${keyword}`);
    }
    // console.log(req.data);
    return req.data;
  } catch (error) {
    throw error;
  }
};

export const editTasks = async (body, id) => {
  try {
    const resp = await client.put(`api/task/${id}`, body);
    return resp.data;
  } catch (error) {
    // console.log(error);
    throw error;
  }
};


