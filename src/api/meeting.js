import client from "./request";

export const getAllMeetings = async (limit, skip, keyword) => {
  try {
    let req;
    if (!limit) {
      req = await client.get(`api/meeting?s=${keyword}`);
    } else {
      req = await client.get(`api/meeting?limit=${limit}&skip=${skip}&s=${keyword}`);
    }
    console.log(req.data);
    return req.data;
  } catch (error) {
    throw error;
  }
};

export const getMeetingDetails = async (id) => {
  try {
    const req = await client.get(`api/meeting/${id}`);
    // console.log(req.data);
    return req.data;
  } catch (error) {
    throw error;
  }
};

export const createMeeting = async (body) => {
  try {
    const resp = await client.post(`api/meeting`, body);
    // console.log(resp.data);
    return resp.data;
  } catch (error) {
    // console.log(error);
    throw error;
  }
};

export const editMeeting = async (body, id) => {
  try {
    const resp = await client.put(`api/meeting/${id}`, body);
    // console.log(resp.data);
    return resp.data;
  } catch (error) {
    // console.log(error);
    throw error;
  }
};

export const deleteMeeting = async (id) => {
  try {
    const resp = await client.delete(`api/meeting/${id}`);
    // console.log(resp.data);
    return resp.data;
  } catch (error) {
    // console.log(error);
    throw error;
  }
};

