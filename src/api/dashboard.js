import client from "./request";

export const getAllDashboard = async (date) => {
  try {
    let req = await client.get(`api/dashboard?date=${date}`);
    // console.log(req.data);
    return req.data;
  } catch (error) {
    throw error;
  }
};

