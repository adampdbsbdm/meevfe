import client from "./request";

export const getAllUser = async (limit, skip, keyword) => {
  try {
    // const req = await client.get("api/audience/user");
    let req;
    if (!limit) {
      req = await client.get(`api/audience/user?s=${keyword || ''}`);
    } else {
      req = await client.get(`api/audience/user?limit=${limit}&skip=${skip}&s=${keyword || ''}`);
    }
    return req.data;
  } catch (error) {
    throw error
  }
};

export const createUser = async (body) => {
  try {
    const resp = await client.post(`api/audience/user`, body);
    return resp.data;
  } catch (error) {
    throw error;
  }
};

export const editUser = async (body, id) => {
  try {
    const resp = await client.put(`api/audience/user/${id}`, body);
    return resp.data;
  } catch (error) {
    throw error;
  }
};

export const deleteUser = async (id) => {
  try {
    const resp = await client.delete(`api/audience/user/${id}`);
    return resp.data;
  } catch (error) {
    throw error;
  }
};

export const changeUserPassword = async (body, id) => {
  try {
    const resp = await client.put(`api/audience/user/change/${id}`, body);
    return resp.data;
  } catch (error) {
    throw error;
  }
};

export const getAllGroup = async (limit, skip, keyword) => {
  try {
    // const req = await client.get("api/audience/group");
    let req;
    if (!limit) {
      req = await client.get(`api/audience/group?s=${keyword || ''}`);
    } else {
      req = await client.get(`api/audience/group?limit=${limit}&skip=${skip}&s=${keyword || ''}`);
    }
    return req.data;
  } catch (error) {
    throw error
  }
};

export const createGroup = async (body) => {
  try {
    const resp = await client.post(`api/audience/group`, body);
    return resp.data;
  } catch (error) {
    throw error;
  }
};

export const editGroup = async (body, id) => {
  try {
    const resp = await client.put(`api/audience/group/${id}`, body);
    return resp.data;
  } catch (error) {
    throw error;
  }
};

export const deleteGroup = async (id) => {
  try {
    const resp = await client.delete(`api/audience/group/${id}`);
    return resp.data;
  } catch (error) {
    throw error;
  }
};

export const getAllExternal = async (limit, skip, keyword) => {
  try {
    // const req = await client.get("api/audience/external");
    let req;
    if (!limit) {
      req = await client.get(`api/audience/external?s=${keyword || ''}`);
    } else {
      req = await client.get(`api/audience/external?limit=${limit}&skip=${skip}&s=${keyword || ''}`);
    }
    return req.data;
  } catch (error) {
    throw error
  }
};

export const createExternal = async (body) => {
  try {
    const resp = await client.post(`api/audience/external`, body);
    return resp.data;
  } catch (error) {
    throw error;
  }
};

export const editExternal = async (body, id) => {
  try {
    const resp = await client.put(`api/audience/external/${id}`, body);
    return resp.data;
  } catch (error) {
    throw error;
  }
};

export const deleteExternal = async (id) => {
  try {
    const resp = await client.delete(`api/audience/external/${id}`);
    return resp.data;
  } catch (error) {
    throw error;
  }
};
