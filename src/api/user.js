import client from "./request";

export const loginUser = async (body) => {
  try {
    const req = await client.post(`api/auth`, body);
    // console.log(req.data);
    return req.data;
  } catch (error) {
    throw error;
  }
};

export const getAccount = async () => {
  try {
    const req = await client.get(`api/audience/useraccount`);
    // console.log(req.data);
    return req.data;
  } catch (error) {
    throw error;
  }
};

export const changeAccount = async (body) => {
  try {
    const req = await client.put(`api/audience/useraccount`, body);
    return req.data;
  } catch (error) {
    throw error;
  }
};

export const changePassword = async (body) => {
  try {
    const req = await client.post(`api/auth/change`, body);
    return req.data;
  } catch (error) {
    throw error;
    // if (error.response) {
    //   return error.response;
    // } else if (error.request) {
    //   // The request was made but no response was received
    //   throw error;
    // } else {
    //   // Something happened in setting up the request that triggered an Error
    //   throw error;
    // }
  }
};
