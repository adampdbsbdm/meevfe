import client from "./request";

export const uploadFileToFS = async (body) => {
  try {
    const resp = await client.post(`api/file`, body, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
    // console.log(resp.data);
    return resp.data;
  } catch (error) {
    // console.log(error);
    throw error;
  }
};
