import client from "./request";

export const getReportDetails = async (id) => {
  try {
    const req = await client.get(`api/report/${id}`);
    // console.log(req.data);
    return req.data;
  } catch (error) {
    throw error;
  }
};

export const editReport = async (body, id) => {
  try {
    const resp = await client.put(`api/report/${id}`, body);
    // console.log(resp.data);
    return resp.data;
  } catch (error) {
    // console.log(error);
    throw error;
  }
};