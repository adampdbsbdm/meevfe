import axios from "axios";
import { getToken, isLoggedIn } from "../data/user";
import {BASE_URL} from './config';

export let config = {
  headers: { "x-auth-token": getToken() },
};

const client = axios.create();

client.defaults.baseURL =  BASE_URL// "http://127.0.0.1:5000/";
client.defaults.headers.post["Content-Type"] = "application/json";

client.interceptors.request.use(
  (request) => {
    // client.defaults.headers.common['x-auth-token'] = getToken()
    request.headers["x-auth-token"] = getToken();

    return request;
  },
  (error) => {
    console.log(error);
    return Promise.reject(error);
  }
);

client.interceptors.response.use(
  (response) => {
    // console.log(response);
    // Edit response config
    return response;
  },
  (error) => {
    console.log(error);
    return Promise.reject(error);
  }
);

export default client;
