import React, { Fragment, useState, useEffect, createContext } from "react";
import { Link } from "react-router-dom";
import Identity from "../components/Identity";
import Settings from "../components/Settings";
import Notifications from "../components/Notifications";
import styles from "./TopHeader.module.scss";
import TheContent from "./TheContent";
import { Logo1 } from "../components/Logo";
import { isLoggedIn, UserContext, getUser } from "../data/user";
import Login from "../pages/Login";
import swal from "../components/Alert";
import { ToastContainer, toast } from "react-toastify";
import { MEMBER } from "../types/RoleStatus";

export default function TheLayout(props) {
  const [isActive, setActive] = useState(false);

  const [isUserOn, setIsUserOn] = useState();
  const [userData, setUserData] = useState();
  const [loginShow, setLoginShow] = useState(false);

  useEffect(() => {
    storageEventHandler();
    window.addEventListener("storage", storageEventHandler, false);
  }, []);

  // useEffect(() => {
  // }, [userData]);

  function storageEventHandler() {
    setIsUserOn(isLoggedIn());
    setUserData(getUser());
    // console.log(getUser());
  }

  const toggleMenu = () => {
    setActive(!isActive);
  };

  return (
    <UserContext.Provider value={userData}>
      <div className="wrapper">
        {/* <!-- Sidebar --> */}
        <nav id="sidebar" className={isActive ? "active" : ""}>
          <div
            className="sidebar-header d-flex 
            justify-content-center align-items-center mr-2"
          >
            <div style={{ width: 40 }}>
              <Logo1 color="#fcfcfc"></Logo1>
            </div>
            {/* <div
            style={{
              borderRadius: 32,
              backgroundColor: "#A60808",
              width: 32,
              height: 32,
            }}
          ></div> */}
            <p
              style={{
                fontSize: 25,
                fontWeight: "bold",
                margin: "auto 15px",
              }}
            >
              Meev
            </p>
          </div>
          <ul className="list-unstyled components">
            <li
              className={`${
                props?.location.pathname.split("/")[1] === "dashboard"
                  ? "active"
                  : ""
              }`}
            >
              <Link to="/" className="side-navlink">
                <div>
                  <i className="fa fa-table"></i>
                  <span>Dashboard</span>
                </div>
              </Link>
            </li>
            <li
              className={`${
                props?.location.pathname.split("/")[1] === "meeting"
                  ? "active"
                  : ""
              }`}
            >
              <Link to="/meeting" className="side-navlink">
                <div>
                  <i className="fa fa-handshake"></i>
                  <span>Meeting</span>
                </div>
              </Link>
            </li>
            {isUserOn && (
              <li
                className={`${
                  props?.location.pathname.split("/")[1] === "tasks"
                    ? "active"
                    : ""
                }`}
              >
                <Link to="/tasks" className="side-navlink">
                  <div>
                    <i className="fa fa-tasks"></i>
                    <span>Tasks</span>
                  </div>
                </Link>
              </li>
            )}
            {isUserOn &&
              userData?.privilege &&
              userData?.privilege?.toLowerCase() !== MEMBER.toLowerCase() && (
                <li
                  className={`${
                    props?.location.pathname.split("/")[1] === "audiences"
                      ? "active"
                      : ""
                  }`}
                >
                  <Link to="/audiences" className="side-navlink">
                    <div>
                      <i className="fas fa-users"></i>
                      <span>Audiences</span>
                    </div>
                  </Link>
                </li>
              )}
          </ul>
        </nav>
        {/* <!-- Page Content --> */}
        <div id="content" className={isActive ? "active" : ""}>
          <nav className={`navbar navbar-expand-lg ${styles.frame} mb-1`}>
            <div className="container-fluid d-flex justify-content-between">
              <button
                type="button"
                id="sidebarCollapse"
                className="blank-button"
                onClick={toggleMenu}
              >
                <i className="fas fa-bars" style={{ fontSize: "25px" }}></i>
              </button>
              <div className="d-flex align-items-center">
                {isUserOn ? (
                  <Fragment>
                    <Notifications></Notifications>
                    <Identity
                      name={userData?.name}
                      username={userData?.username}
                      email={userData?.email}
                      privilege={userData?.privilege}
                      photo={userData?.photo}
                    ></Identity>
                    <Settings logoutCallback={storageEventHandler}></Settings>
                  </Fragment>
                ) : (
                  <Fragment>
                    <button
                      className="btn btn-custom 
                      btn-primary-custom-holloe btn-smol mr-4 rounded-pill"
                      onClick={() => setLoginShow(true)}
                    >
                      Login
                    </button>
                  </Fragment>
                )}
              </div>
            </div>
          </nav>

          <TheContent></TheContent>
        </div>

        <ToastContainer />
        <Login
          isShow={loginShow}
          onChange={setLoginShow}
          onSuccess={storageEventHandler}
        ></Login>
      </div>
    </UserContext.Provider>
  );
}
