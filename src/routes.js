import React from "react";

const Dashboard = React.lazy(() => import("./pages/Dashboard"));
const CEMeeting = React.lazy(() => import("./pages/CEMeeting"));
const CEReport = React.lazy(() => import("./pages/CEReport"));
const Meeting = React.lazy(() => import("./pages/Meeting"));
const MeetingDetails = React.lazy(() => import("./pages/MeetingDetails"));
const Tasks = React.lazy(() => import("./pages/Tasks"));
const Audiences = React.lazy(() => import("./pages/Audiences"));

const routes = [
  { path: "/", exact: true, name: "Home" },
  { path: "/dashboard", name: "Dashboard", component: Dashboard },
  { path: "/meeting", name: "Meeting", component: Meeting, exact: true },
  {
    path: "/meeting/create",
    name: "Create Meeting",
    component: CEMeeting,
    exact: true,
    private:true
  },
  {
    path: "/meeting/create/:datestart",
    name: "Create Meeting",
    component: CEMeeting, private:true
  },
  { path: "/meeting/edit/:id", name: "Edit Meeting", component: CEMeeting, private:true },
  {
    path: "/meeting/detail/:id",
    name: "Detail Meeting",
    component: MeetingDetails,
  },
  { path: "/meeting/report/:id", name: "Report", component: CEReport, private:true },
  { path: "/tasks", name: "Tasks", component: Tasks, exact: true, private:true },
  { path: "/audiences", name: "Audiences", component: Audiences, exact: true, private:true, admin:true },
];

export default routes;
