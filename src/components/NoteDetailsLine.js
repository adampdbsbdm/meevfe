import React, { Fragment } from "react";

export default function NoteDetailsLine({ id, point, notes }) {
  return (
    <div className="row mx-0 col-12 py-4 px-0 row-list">
      <div className="row mx-0 col-12">
        <div className="col-4">
          <span>{point}</span>
        </div>

        <div className="col-8">
          <span>{notes}</span>
        </div>
      </div>
    </div>
  );
}
