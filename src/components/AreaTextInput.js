import React, { Fragment, useState, useEffect } from "react";

export default function AreaTextInput({
  caption,
  name,
  value,
  change,
  placeholder,
  rows,
  required,
}) {
  const [thevalue, setThevalue] = useState("");

  // useEffect(() => {
  //   setThevalue(value);
  // }, []);

  useEffect(() => {
    setThevalue(value);
  }, [value]);

  function onChange(e) {
    setThevalue(e.target.value);
    change({ key: name, value: e.target.value });
  }

  return (
    <Fragment>
      <label
        className="col-sm-12 col-md-2"
        style={{ marginTop: "0.0em" }}
        htmlFor={name}
      >
        {caption}
        {required ? <span className="text-danger ml-0">*</span> : ""}
      </label>
      <div className="col-sm-12 col-md-10">
        <textarea
          className="form-control"
          id={name}
          name={name}
          onChange={onChange}
          value={thevalue}
          placeholder={placeholder}
          rows={rows}
          required={required}
        />
        {required ? (
          <div className="invalid-feedback">Field is required</div>
        ) : (
          ""
        )}
      </div>
    </Fragment>
  );
}
