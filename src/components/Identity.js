import React, { useEffect } from "react";
import styles from "./Identity.module.scss";

export default function Identity({ name, username, email, privilege, photo }) {
  return (
    <div className="d-flex align-items-center mx-4">
      <div
        className="rounded-circle"
        style={{ width: 40, height: 40, overflow: "hidden" }}
      >
        {/* <img style={{ width: 40, height: 40 }} src={photo} /> */}
        <img
          style={{ width: 40, height: 40 }}
          src={
            photo ||
            `https://ui-avatars.com/api/?name=${
              name || username || "User"
            }&size=64&rounded=true&background=random&bold=true`
          }
        />
      </div>
      <div style={{ marginLeft: 20 }}>
        <p className={`font-weight-bold ${styles.text}`}>
          {name || username || "User"}
        </p>
        <p className={styles.text}>{email}</p>
      </div>
    </div>
  );
}
