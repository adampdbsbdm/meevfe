import React, { Fragment, useState, useEffect } from "react";

import CreatableSelect from "react-select/creatable";
import chroma from "chroma-js";
import Select from "react-select";

export default function SingleDataSelectInput({
  caption,
  name,
  value,
  availableOptions,
  change,
  placeholder,
  required,
  creatable,
}) {
  const [selectOptions, setSelectOptions] = useState(availableOptions);

  const [thevalue, setTheValue] = useState();

  // useEffect(() => {
  //   console.log(value);
  //   setTheValue(value);
  // }, []);

  useEffect(() => {
    setTheValue(value);
  }, [value]);

  useEffect(() => {
    // console.log(availableOptions);
    setSelectOptions(availableOptions);
  }, [availableOptions]);

  const colourStyles = {
    control: (styles) => ({
      ...styles,
      backgroundColor: "white",
    }),
    option: (styles, { data, isDisabled, isFocused, isSelected }) => {
      data.color = data.color || "#4B4B4D";
      const color = chroma(data.color);
      return {
        ...styles,

        fontSize: "0.9em",
        backgroundColor: isSelected
          ? data.color
          : isFocused
          ? color.alpha(0.1).css()
          : null,
        color: isSelected
          ? chroma.contrast(color, "white") > 2
            ? "white"
            : "black"
          : data.color,

        ":active": {
          ...styles[":active"],
          backgroundColor:
            !isDisabled && (isSelected ? data.color : color.alpha(0.3).css()),
        },
      };
    },
  };

  function updateValue(e, actionMeta) {
    if (actionMeta.action === "create-option") {
      e.value = ""
    }

    const selected = {
      // key: e?.label || "",
      key: name || "",
      value: e?.value || "",
      label: e?.label || "",
    };
    setTheValue(selected);
    console.log(selected);
    change(selected);
  }

  const handleInputChange = (inputValue, actionMeta) => {
    // console.log(actionMeta.action, inputValue);
    // if (actionMeta.action === "create-option") {
    //   inputValue.value = ""
    //   updateValue(inputValue);
    // }
  };

  return (
    <Fragment>
      {caption ? (
        <label
          className="col-sm-12 col-md-2"
          style={{ marginTop: "0.5em" }}
          htmlFor={name}
        >
          {caption}
          {required ? <span className="text-danger ml-0">*</span> : ""}
        </label>
      ) : (
        ""
      )}

      <div className={`col-sm-12 ${caption ? "col-md-10" : "px-0"}`}>
        {!creatable ? (
          <Select
            options={selectOptions}
            styles={colourStyles}
            name={name}
            placeholder={placeholder}
            classNamePrefix="select"
            onChange={updateValue}
            value={thevalue}
            style={{ borderColor: "#dc3545" }}
          />
        ) : (
          <CreatableSelect
            isClearable
            options={selectOptions}
            styles={colourStyles}
            name={name}
            placeholder={placeholder}
            classNamePrefix="select"
            onChange={updateValue}
            onInputChange={handleInputChange}
            value={thevalue}
            style={{ borderColor: "#dc3545" }}
          />
        )}
        {required && !thevalue?.value ? (
          <div className="invalid-feedback custom-invalid">
            Field is required
          </div>
        ) : (
          ""
        )}
      </div>
    </Fragment>
  );
}
