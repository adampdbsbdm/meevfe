import React, { Fragment, useState, useEffect, useRef } from "react";
import { uploadFileToFS } from "../api/file";
import client from "../api/request";
import pp from "../assets/pp.jpg";

export default function PhotoUpload({
  caption,
  name,
  value,
  change,
  className,
  spacing = [2, 10],
}) {
  const [thevalue, setThevalue] = useState("");
  const uploadEl = useRef(null);

  useEffect(() => {
    setThevalue(value);
  }, [value]);

  function onChange(path) {
    setThevalue(path);
    change({ key: name, value: path });
  }

  const deleteItem = () => {
    onChange("")
  };

  const onSelectUpload = (e) => {
    uploadFile(uploadEl.current.files[0]);
  };

  const uploadFile = async (file) => {
    if (file.size > 2000000) {
      alert("File is too big!");
      uploadEl.current.value = "";
      return;
    }

    try {
      const upForm = new FormData();
      upForm.append("file", file);
      const resp = await uploadFileToFS(upForm);
      onChange(`${client.defaults.baseURL}${resp.path}`);
    } catch (err) {
      console.error(err);
    }
  };

  return (
    <Fragment>
      <label
        className={`col-sm-12 col-md-${spacing[0]} ${className}`}
        style={{ marginTop: "0.5em" }}
        htmlFor={name}
      >
        {caption}
      </label>
      <div className={`col-sm-12 col-md-${spacing[1]} flex-columns my-2`}>
        <div>
          <button
            className="btn btn-secondary btn-sm"
            type="button"
            onClick={() => {
              uploadEl.current.click();
            }}
          >
            Upload
          </button>
          {thevalue && (
            <button
              className="btn btn-danger btn-sm ml-1"
              type="button"
              onClick={deleteItem}
            >
              Delete
            </button>
          )}

          <input
            hidden
            type="file"
            ref={uploadEl}
            id="fileupload"
            name="file"
            accept="image/*"
            onChange={onSelectUpload}
          />
        </div>
        <div className={`mt-3`}>
          <img
            src={thevalue || pp}
            height="auto"
            width={150}
            style={{ objectFit: "cover", border: "dashed gray 1px"}}
          ></img>
        </div>
      </div>
    </Fragment>
  );
}
