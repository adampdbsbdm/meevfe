import React, { Fragment } from "react";

export default function DecisionDetailsLine({ id, pic, detail, decision, due }) {
  return (
    <div className="row mx-0 col-12 py-4 px-0 row-list">
      <div className="row mx-0 col-12">
        <div className="col-3">
          <span>{pic}</span>
        </div>

        <div className="col-3">
          <span>{decision}</span>
        </div>
        <div className="col-3">
          <span>{detail}</span>
        </div>
        <div className="col-3">
          <span>{due}</span>
        </div>
      </div>
    </div>
  );
}
