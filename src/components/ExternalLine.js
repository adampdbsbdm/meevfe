import React, { Fragment, useState, useEffect } from "react";
import swal, { getConfirmConfig } from "./Alert";
import styles from "./TextOutput.module.scss";

export default function ExternalLine({
  id,
  name,
  information,
  contact,
  email,
  showUpdate,
  onDelete,
  onChange,
  onUpdate,
  editMode,
  onEditModeChange
}) {
  const [line, setLine] = useState({
    id: "",
    name: "",
    information: "",
    contact: "",
    email: "",
    anyUpdate: false,
    editMode: false
  });
  const [prevline, setPrevline] = useState();
  const [anyUpdate, setAnyUpdate] = useState(false);

  useEffect(() => {
    setLine({ id, name, information, contact, email, anyUpdate, editMode });
    setPrevline({ id, name, information, contact, email, anyUpdate, editMode })
  }, []);

  useEffect(() => {
    // console.log("asdasdasd", showUpdate, line.id, id);
    setAnyUpdate(showUpdate);
  }, [showUpdate]);

  // useEffect(() => {
  //   console.log("abc", anyUpdate, line.id, id);
  // }, [anyUpdate]);

  async function deleteLine() {
    const result = await swal.fire(
      getConfirmConfig(
        "Delete External?",
        "Do you want to delete external audiences?"
      )
    );
    if (result.isConfirmed) {
      onDelete(line.id);
    }
  }

  async function updateLine() {
    await onUpdate(line.id); 
    const newline = {
      ...line,
      isEdit: false,
    };
    setLine(newline);
    setPrevline(newline)
    onChange(newline);
    onEditModeChange(-1);
  }

  function enableEdit() {
    setPrevline({...line})
    const newline = {
      ...line,
      isEdit: true,
    };
    setLine(newline);
    onChange(newline);
    onEditModeChange(1);
  }

  function undoEdit() {
    const prev = {...prevline, isEdit: false}
    setLine(prev);
    onChange(prev);
    onEditModeChange(-1);
  }

  return (
    <div className="row mx-0 col-12 py-4 px-0 row-list">
      <div className="row mx-0 col-11">
        <div className="col-3 d-flex align-items-center">
          {!editMode ? (
            <p className={`${styles.textcontent}`}>{name}</p>
          ) : (
            <>
              <input
                className="form-control"
                id={`extname${id}`}
                type="text"
                name={`extname${id}`}
                onChange={(e) => {
                  const newline = {
                    ...line,
                    name: e.target.value,
                    anyUpdate: true,
                  };
                  setLine(newline);
                  onChange(newline);
                }}
                value={name}
                required
              />
              <div className="invalid-feedback">Field is required</div>
            </>
          )}
        </div>
        <div className="col-3 d-flex align-items-center">
          {!editMode ? (
            <p className={`${styles.textcontent}`}>{information || "-"}</p>
          ) : (
              <textarea
                className="form-control"
                id={`extinformation${id}`}
                type="text"
                name={`extinformation${id}`}
                onChange={(e) => {
                  const newline = {
                    ...line,
                    information: e.target.value,
                    anyUpdate: true,
                  };
                  setLine(newline);
                  onChange(newline);
                }}
                value={information}
                required
                rows={1}
              />
          )}
        </div>
        <div className="col-3 d-flex align-items-center">
          {!editMode ? (
            <p className={`${styles.textcontent}`}>{contact || "-"}</p>
          ) : (
            <div>
              <input
                className="form-control"
                id={`extcontact${id}`}
                type="text"
                name={`extcontact${id}`}
                onChange={(e) => {
                  const newline = {
                    ...line,
                    contact: e.target.value,
                    anyUpdate: true,
                  };
                  setLine(newline);
                  onChange(newline);
                }}
                value={contact}
                required
              />
              <div className="invalid-feedback">Field is required</div>
            </div>
          )}
        </div>
        <div className="col-3 d-flex align-items-center">
          {!editMode ? (
            <p className={`${styles.textcontent}`}>{email || "-"}</p>
          ) : (
            <div>
              <input
                className="form-control"
                id={`extemail${id}`}
                type="text"
                name={`extemail${id}`}
                onChange={(e) => {
                  const newline = {
                    ...line,
                    email: e.target.value,
                    anyUpdate: true,
                  };
                  setLine(newline);
                  onChange(newline);
                }}
                value={email}
                required
              />
              <div className="invalid-feedback">Field is required</div>
            </div>
          )}
        </div>
      </div>
      <div className="col-1 row">
        {!editMode ? (
          <>
            <button
              type="button"
              title="Delete"
              key="gantidong-delete"
              className="blank-button text-danger 
                d-flex align-items-center justify-content-center mx-2 my-1 px-0"
              onClick={deleteLine}
            >
              <i className="fas fa-trash-alt" style={{ fontSize: "1em" }}></i>
            </button>
            <button
              type="button"
              title="Edit"
              key="gantidong-edit"
              className="blank-button text-secondary 
                d-flex align-items-center justify-content-center mx-2 my-1 px-0"
              onClick={enableEdit}
            >
              <i className="fas fa-edit" style={{ fontSize: "1em" }}></i>
            </button>
          </>
        ) : (
          <>
            <button
              type="button"
              title="Update"
              key="gantidong-update"
              className="blank-button text-success 
                d-flex align-items-center justify-content-center mx-2 my-1 px-0"
              onClick={updateLine}
            >
              <i className="fa fa-check" style={{ fontSize: "1em" }}></i>
            </button>
            <button
              type="button"
              title="Cancel"
              key="gantidong-cancel"
              className="blank-button text-secondary 
                d-flex align-items-center justify-content-center mx-2 my-1 px-0"
              onClick={undoEdit}
            >
              <i className="fas fa-times" style={{ fontSize: "1em" }}></i>
            </button>
          </>
        )}
      </div>
    </div>
  );
}

/* {anyUpdate && (
          <button
            type="button"
            className="blank-button text-success d-flex align-items-center justify-content-center col-1"
            onClick={updateLine}
          >
            <i className="fa fa-check" style={{ fontSize: "1em" }}></i>
          </button>
        )} */
