import React, { useState, useEffect } from "react";
import SimpleDatetimePicker from "./SimpleDatetimePicker";
import SingleDataSelectInput from "./SingleDataSelectInput";

export default function PICLine({
  id,
  name,
  task,
  due,
  fullline,
  availableName,
  onDelete,
  onChange,
}) {
  const [line, setLine] = useState({
    ...fullline,
    id,
    name,
    task,
    due
  });

  useEffect(() => {
    setLine({ ...fullline, id, name, task, due });
  }, []);

  function updateValue(newline) {
    setLine(newline);
    onChange(newline);
  }

  function memberChanged(e) {
    // console.log(e);
    const newline = { ...line, name: { id: e.value, name: e.label } };
    setLine(newline);
    onChange(newline);
  }

  function deleteLine() {
    onDelete(line);
  }

  return (
    <div className="row mx-0 col-12 py-4 px-0 row-list">
      <div className="row mx-0 col-11">
        <div className="col-4">
          <SingleDataSelectInput
            name="name"
            placeholder="Select pic name..."
            value={name}
            change={memberChanged}
            required={true}
            availableOptions={availableName?.map((o, i) => {
              return { value: o.value, label: o.label };
            })}
          ></SingleDataSelectInput>
        </div>

        <div className="col-4">
          <textarea
            className="form-control"
            id={`taskpic${id}`}
            type="text"
            name={`taskpic${id}`}
            onChange={(e) => {
              console.log(name);
              const newline = {
                ...line,
                name: { id: name.value, name: name.label },
                task: e.target.value,
              };
              updateValue(newline);
            }}
            value={line.task}
            required={true}
            rows={1}
          />
          <div className="invalid-feedback">Field is required</div>
        </div>

        <div className="col-4">
          <SimpleDatetimePicker
            name="due"
            caption=""
            placeholder="Select datetime"
            value={due}
            change={(e) => {
              const newline = {
                ...line,
                name: { id: name.value, name: name.label },
                due: e.value,
              };
              updateValue(newline);
            }}
            disableTime={false}
            required={true}
          ></SimpleDatetimePicker>
        </div>
      </div>
      <button
        type="button"
        className="blank-button text-danger d-flex align-items-center justify-content-center col-1"
        onClick={deleteLine}
      >
        <i className="fas fa-trash-alt" style={{ fontSize: "1em" }}></i>
      </button>
    </div>
  );
}
