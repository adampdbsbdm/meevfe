import React, { Fragment, useState, useEffect } from "react";

export default function SimpleComboBox({
  caption,
  name,
  value,
  change,
  list,
  placeholder,
  required,
}) {
  const [thevalue, setThevalue] = useState("");

  useEffect(() => {
    setThevalue(value);
  }, []);

  return (
    <Fragment>
      <label
        className="col-sm-12 col-md-2"
        style={{ marginTop: "0.5em" }}
        htmlFor={name}
      >
        {caption}
        {required ? <span className="text-danger ml-0">*</span> : ""}
      </label>
      <div className="col-sm-12 col-md-10">
        <select
          className="custom-select"
          name={name}
          onChange={(e) => {
            setThevalue(e.target.value);
            change(e);
          }}
          id={name}
          value={thevalue}
        >
          <option defaultValue={0} disabled>
            {placeholder}
          </option>
          {list.map((o, i) => (
            <option value={o.name} key={i}>
              {o.name}
            </option>
          ))}
        </select>
        {required ? (
          <div className="invalid-feedback">Field is required</div>
        ) : (
          ""
        )}
      </div>
    </Fragment>
  );
}
