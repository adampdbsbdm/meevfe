import React, { Fragment, useState, useEffect } from "react";
import { Link, useHistory } from "react-router-dom";
import { getDateString } from "../helper/dateparser";
import * as MeetingStatusType from "../types/MeetingStatus";
import { ADMIN, EC, MEMBER } from "../types/RoleStatus";
import swal, { getConfirmConfig } from "./Alert";

export default function MeetingTableLine({
  id,
  event,
  datestart,
  dateend,
  pic,
  attachment,
  status,
  onDelete,
  onChange,
  creator,
  user,
  baseurl
}) {
  // const [line, setLine] = useState({
  //   id,
  //   event,
  //   datestart,
  //   dateend,
  //   pic,
  //   attachment,
  //   status,
  // });
  const [line, setLine] = useState({});
  const [isActive, setActive] = useState(false);
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [userData, setUserData] = useState(false);
  let history = useHistory();

  useEffect(() => {
    const uid = user?.id || null;
    const aspic = pic?.find((i) => uid === i.aid) ? "Yes" : "No";
    setLine({
      id,
      event,
      datestart: getDateString(new Date(datestart)),
      dateend: getDateString(new Date(dateend)),
      aspic,
      attachment,
      status,
      creator,
    });
    setIsLoggedIn(user ? true : false);
    setUserData({ ...user });
  }, []);

  // Tidak perlu, diganti refresh page ketika login/logout
  // useEffect(() => {
  //   setIsLoggedIn(user ? true : false);
  //   setUserData({ ...user });
  //   console.log({...user});
  // }, [user]);

  // useEffect(() => {
  //   updateValue();
  // });

  function updateValue() {
    console.log(line);
    onChange(line);
  }

  async function deleteLine() {
    const result = await swal.fire(
      getConfirmConfig("Delete Meeting?", "Do you want to delete meeting?")
    );
    if (result.isConfirmed) {
      onDelete(line.id);
    }
  }

  const toggleMenu = (e) => {
    setActive(!isActive);
  };

  const disableMenu = (e) => {
    if (isActive) {
      setActive(false);
    }
    window.removeEventListener('click', disableMenu);
  };

  // ref : https://stackoverflow.com/questions/57944794/react-hooks-setstate-does-not-update-state-properties
  useEffect(() => {
    // window.addEventListener("click", disableMenu);
    // return () => window.removeEventListener("click", disableMenu);
    setTimeout(() => {
      if(isActive){
        window.addEventListener('click', disableMenu)
      }
    }, 0)
  });

  return (
    <div className="row mx-0 col-12 py-4 px-0 row-list">
      <div className="row mx-0 col-11">
        <div className="col-3">
          <span>{line.event}</span>
        </div>

        {isLoggedIn ? (
          <Fragment>
            <div className="col-2">
              <span>{line.datestart}</span>
            </div>
            <div className="col-2">
              <span>{line.dateend}</span>
            </div>
            <div className="col-2">
              <span>{line.aspic}</span>
            </div>
          </Fragment>
        ) : (
          <Fragment>
            <div className="col-3">
              <span>{line.datestart}</span>
            </div>
            <div className="col-3">
              <span>{line.dateend}</span>
            </div>
          </Fragment>
        )}
        <div className="col-2">
          {line.attachment ? (
            <div className="d-flex align-items-center justify-content-start pointer">
              <a
                href={`${baseurl}${line.attachment}`}
                target="_blank"
                className="d-flex align-items-center text-primary"
              >
                <i
                  className="fas fa-download"
                  style={{ fontSize: "1em", marginRight: 5 }}
                ></i>
                <span> Download</span>
              </a>
            </div>
          ) : (
            // <a href={line.attachment} className="text-primary">
            //   Download
            // </a>
            <span>None</span>
          )}
        </div>
        <div className="col-1">
          <span
            className={`badge ${
              line.status === MeetingStatusType.NYS
                ? "badge-primary"
                : line.status === MeetingStatusType.CANCEL
                ? "badge-danger"
                : "badge-success"
            }`}
            style={{ width: "80px" }}
          >
            {line.status}
          </span>
        </div>
      </div>

      <div className="dropdown row col-1 align-items-center justify-content-end pr-1">
        <button
          type="button"
          className="blank-button d-flex align-items-center justify-content-center"
          onClick={toggleMenu}
        >
          <i className="fas fa-ellipsis-v" style={{ fontSize: "1em" }}></i>
        </button>
        <div
          className={`dropdown-menu ${isActive ? "d-block" : "d-none"}`}
          style={{ top: 25, right: 10, left: "auto" }}
        >
          {isLoggedIn &&
            (userData?.privilege?.toLowerCase() === ADMIN.toLowerCase() ||
              (userData?.privilege?.toLowerCase() === EC.toLowerCase() &&
                userData?.id === creator)) && (
              <Link
                className="dropdown-item mb-1 small-menu"
                to={"/meeting/edit/" + id}
              >
                Edit
              </Link>
            )}
          {isLoggedIn &&
            (userData?.privilege?.toLowerCase() === ADMIN.toLowerCase() ||
              (userData?.privilege?.toLowerCase() === EC.toLowerCase() &&
                userData?.id === creator)) && (
              <Link
                className="dropdown-item mb-1 small-menu"
                to={"/meeting/report/" + id}
              >
                Report
              </Link>
            )}

          <Link
            className="dropdown-item mb-1 small-menu"
            to={"/meeting/detail/" + id}
          >
            Detail
          </Link>
          {isLoggedIn &&
            (userData?.privilege?.toLowerCase() === ADMIN.toLowerCase() ||
              (userData?.privilege?.toLowerCase() === EC.toLowerCase() &&
                userData?.id === creator)) && (
              <button
                className="dropdown-item text-danger small-menu blank-button"
                onClick={deleteLine}
              >
                Delete
              </button>
            )}
        </div>
      </div>
    </div>
  );
}
