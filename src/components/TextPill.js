import React, { Fragment } from "react";
import styles from "./TextOutput.module.scss";

export default function TextPill({ value, bgcolor, isError = false }) {
  return (
    <>
      <span
        className={`badge badge-pill badge-light mr-1 font-weight-normal ${
          !isError ? styles.multitextcontent : styles.multitextcontentdanger
        }`}
        style={{background:bgcolor}}
      >
        {value}
      </span>
    </>
  );
}
