import React, { Fragment } from "react";

export default function SearchBar({ setKeyword, placeholder}) {
  function onChange(e) {
    setKeyword(e.target.value);
  }

  return (
    <Fragment>
      <div className="d-flex align-items-center">
        <label
          className="font-weight-bold mr-2"
          style={{ marginTop: "0.5em" }}
          htmlFor="searchbar"
        >
          Search
        </label>
        <div>
          <input
            className="form-control"
            type="text"
            name="searchbar"
            onChange={onChange}
            placeholder={placeholder}
          />
        </div>
      </div>
    </Fragment>
  );
}
