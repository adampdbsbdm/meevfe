import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'

const swal = withReactContent(Swal)
export default swal

export function getConfirmConfig(title, text, type="warning") {
    const config_confirm = {
        title,
        text,
        icon: type,
        showCancelButton: true,
        confirmButtonColor: "#d33",
        cancelButtonColor: "#cccdd0",
        confirmButtonText: "Yes",
      }
    return config_confirm
}

export function getErrorConfig(title, text) {
    const config_confirm = {
        title,
        text,
        icon: "error",
        confirmButtonColor: "#d33",
        confirmButtonText: "Yes",
      }
    return config_confirm
}

export function getInputConfig(title, label, placeholder, input) {
  const config_confirm = {
    title: title,
    input: input,
    inputLabel: label,
    inputPlaceholder: placeholder,
    showCancelButton: true,
    cancelButtonColor: "#cccdd0",
    confirmButtonColor: "#007bff",
  }
  return config_confirm
}