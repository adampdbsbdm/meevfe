import React, { Fragment } from "react";
import styles from "./TextOutput.module.scss";

export default function MultiTextOutput({ caption, value, isError = false }) {
  return (
    <Fragment>
      <p
        className={`col-sm-12 col-md-2 ${
          !isError ? styles.textlabel : styles.textlabeldanger
        }`}
      >
        {caption}
      </p>
      <div className={`col-sm-12 col-md-10 row mx-0 flex-column`}>
        {value?.map((v, i) => {
          return (
            <p
              key={i}
              className={`${
                !isError
                  ? styles.multitextcontent
                  : styles.multitextcontentdanger
              }`}
            >
              {v}
            </p>
          );
        })}
      </div>
    </Fragment>
  );
}
