import React, { Fragment, useState, useEffect } from "react";

export default function LocationTextInput({
  offlineloc,
  onlineloc,
  change,
  placeholder,
}) {
  const [offline, setOffline] = useState("");
  const [online, setOnline] = useState("");
  const [enableOnline, setEnableOnline] = useState(true);
  const [enableOffline, setEnableOffline] = useState(false);

  // useEffect(() => {
  //   setEnableOnline(onlineloc?true:false);
  //   setEnableOffline(offlineloc?true:enableOffline);
  // }, []);

  // useEffect(() => {
  //   setOffline(offlineloc);
  //   setOnline(onlineloc);
  // }, [offlineloc, onlineloc]);

  useEffect(() => {
    setOffline(offlineloc);
    if (offlineloc) setEnableOffline(true);
  }, [offlineloc]);

  useEffect(() => {
    setOnline(onlineloc);
    if (onlineloc) setEnableOnline(true);
  }, [onlineloc]);

  function onChange(e) {
    if (e.target.name === "location_offline") {
      setOffline(e.target.value)
      change({ key: e.target.name, value: e.target.value });
    } else if (e.target.name === "location_online"){
      setOnline(e.target.value)
      change({ key: e.target.name, value: e.target.value });
    } else if (e.target.name === "enable_online"){
      setEnableOnline(e.currentTarget.checked)
      change({ key: e.target.name, value: e.currentTarget.checked });
    } else if (e.target.name === "enable_offline"){
      setEnableOffline(e.currentTarget.checked)
      change({ key: e.target.name, value: e.currentTarget.checked });
    }
  }

  return (
    <Fragment>
      <label
        className="col-sm-12 col-md-2"
        style={{ marginTop: "0.0em" }}
        htmlFor="location"
      >
        Location
        <span className="text-danger ml-0">*</span>
      </label>
      <div className="col-sm-12 col-md-10">
        <div style={{zIndex: "auto"}} className="custom-control custom-checkbox custom-checkbox-red form-control-xs mb-2">
          <input
            type="checkbox"
            className="custom-control-input custom-control-input-red"
            id="enable_offline"
            name="enable_offline"
            checked={enableOffline}
            onChange={onChange}
          />
          <label
            className="custom-control-label"
            style={{ color: "#212529" }}
            htmlFor="enable_offline"
          >
            Offline
          </label>
        </div>
        <input
          className="form-control"
          id="location_offline"
          type="text"
          name="location_offline"
          onChange={onChange}
          value={offline}
          placeholder={placeholder}
          disabled={!enableOffline}
          required={enableOffline}
        />
        {enableOffline && !offline ? (
          <div className="invalid-feedback">Field is required</div>
        ) : (
          ""
        )}

        <div style={{zIndex: "auto"}} className="custom-control custom-checkbox custom-checkbox-red mt-4  mb-2 form-control-xs">
          <input
            type="checkbox"
            className="custom-control-input custom-control-input-red"
            id="enable_online"
            name="enable_online"
            checked={enableOnline}
            onChange={onChange}
          />
          <label
            className="custom-control-label"
            style={{ color: "#212529" }}
            htmlFor="enable_online"
          >
            Online
          </label>
        </div>

        <div className="input-group mb-3">
          <div className="input-group-prepend">
            <span className="input-group-text" id="basic-addon1">
              URL
            </span>
          </div>
          <input
            className="form-control"
            id="location_online"
            type="text"
            name="location_online"
            onChange={onChange}
            value={online}
            placeholder={placeholder}
            disabled={!enableOnline}
            required={enableOnline}
          />
        </div>

        {enableOnline && !online ? (
          <div className="invalid-feedback">Field is required</div>
        ) : (
          ""
        )}
      </div>
    </Fragment>
  );
}
