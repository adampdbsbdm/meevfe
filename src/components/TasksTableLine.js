import React, { Fragment, useState, useEffect, useRef } from "react";
import * as TasksStatusType from "../types/TasksStatus";
import { getDateString } from "../helper/dateparser";
import { uploadFileToFS } from "../api/file";
import client from "../api/request";
import { MEMBER } from "../types/RoleStatus";
import { Link } from "react-router-dom";

export default function TasksTableLine({
  id,
  event,
  mid,
  name,
  task,
  evidence,
  status,
  due,
  approved,
  creator,
  onChange,
  onUpdate,
  onEvidence,
  user,
  baseurl
}) {
  const [line, setLine] = useState({
    id,
    event,
    mid,
    name,
    task,
    evidence,
    status,
    due: getDateString(new Date(due || "")),
    approved,
    creator,
  });
  const [isActive, setActive] = useState(false);

  useEffect(() => {
    setLine({
      id,
      event,
      mid,
      name,
      task,
      evidence,
      status,
      due: getDateString(new Date(due || "")),
      approved,
      creator,
    });
  }, []);

  const uploadEl = useRef(null);

  async function changeStatus() {
    toggleMenu();
    const newstatus =
      line.status === TasksStatusType.NY
        ? TasksStatusType.ON_PROGRESS
        : line.status === TasksStatusType.ON_PROGRESS
        ? TasksStatusType.DONE
        : TasksStatusType.ON_PROGRESS;

    await updateLine({ ...line, status: newstatus });
  }

  async function changeApproved() {
    toggleMenu();

    const state = !line.approved;
    const status = state ? TasksStatusType.DONE : line.status;
    await updateLine({ ...line, approved: state, status });
  }

  async function deleteEvidence() {
    toggleMenu();
    await updateLine({ ...line, evidence: "" });
  }

  async function updateLine(newline) {
    const scs = await onUpdate(newline);
    if (scs) {
      setLine(newline);
      onChange(newline);
    }
  }

  const toggleMenu = (e) => {
    console.log(isActive);
    setActive(!isActive);
  };

  const disableMenu = (e) => {
    console.log('a');
    if (isActive) {
      setActive(false);
    }
    window.removeEventListener('click', disableMenu);
  };

  const onSelectUpload = (e) => {
    uploadFile(uploadEl.current.files[0]);
  };

  const uploadFile = async (file) => {
    if (file.size > 50000000) {
      alert("File is too big!");
      uploadEl.current.value = "";
      return;
    }

    try {
      const upForm = new FormData();
      upForm.append("file", file);
      const resp = await uploadFileToFS(upForm);
      await updateLine({
        ...line,
        // evidence: `${client.defaults.baseURL}${resp.path}`,
        evidence: `${resp.path}`,
      });
    } catch (err) {
      console.error(err);
    }
  };
  
  // ref : https://stackoverflow.com/questions/57944794/react-hooks-setstate-does-not-update-state-properties
  useEffect(() => {
    // window.addEventListener("click", disableMenu);
    // return () => window.removeEventListener("click", disableMenu);
    setTimeout(() => {
      if(isActive){
        window.addEventListener('click', disableMenu)
      }
    }, 0)
  });


  return (
    <div className="row mx-0 col-12 py-4 px-0 row-list">
      <div className="row mx-0 w-100">
        <div className="row mx-0 px-4" style={{flexGrow: 1}}>
          <div className="col-2">
            <span>
              <Link to={`/meeting/detail/${line.mid}`} className='default'>
                {line.event}
              </Link>
            </span>
          </div>
          <div className="col-2 col-xxl-3">
            <span>{line.task}</span>
          </div>
          <div className="col-1 col-xxl-2">
            <span>{line.name}</span>
          </div>
          <div className="col-2">
            <span>{line.due}</span>
          </div>
          <div className="col-2 col-xxl-1">
            {line.evidence ? (
              <div className="d-flex align-items-center justify-content-start">
                <a
                  href={`${baseurl}${line.evidence}`}
                  className="d-flex align-items-center text-primary"
                >
                  <i
                    className="fas fa-download"
                    style={{ fontSize: "1em", marginRight: 5 }}
                  ></i>
                  <span> Download</span>
                </a>
              </div>
            ) : (
              <div className="d-flex align-items-center justify-content-start">
                <button
                  type="button"
                  className="blank-button"
                  onClick={() => {
                    uploadEl.current.click();
                  }}
                >
                  <a className="d-flex align-items-center text-primary">
                    <i
                      className="fas fa-upload"
                      style={{ fontSize: "1em", marginRight: 5 }}
                    ></i>
                    <span> Upload</span>
                  </a>
                </button>

                <input
                  hidden
                  type="file"
                  ref={uploadEl}
                  id="fileupload"
                  name="file"
                  // accept=".zip"
                  // onChange={(e) =>
                  //   setLine({
                  //     ...line,
                  //     evidence: "https://www.7-zip.org/a/7za920.zip",
                  //   })
                  // }

                  onChange={onSelectUpload}
                />
              </div>
            )}
          </div>
          <div className="col-2 col-xxl-1">
            <span
              className={`badge ${
                line.status === TasksStatusType.ON_PROGRESS
                  ? "badge-primary"
                  : line.status === TasksStatusType.NY
                  ? "badge-warning"
                  : "badge-success"
              }`}
              style={{ width: "85px" }}
            >
              {line.status}
            </span>
          </div>
          <div className="col-1 d-flex justify-content-center">
            {line.approved ? (
              <span className="text-success" key="uniqueApproved">
                <i
                  className="fas fa-check-circle"
                  style={{ fontSize: "1em" }}
                ></i>
              </span>
            ) : (
              <div className="text-danger" key="uniqueUnApproved">
                <i
                  className="fas fa-times-circle"
                  style={{ fontSize: "1em" }}
                ></i>
              </div>
            )}
          </div>
        </div>

        <div className="dropdown row align-items-start
           justify-content-end pr-1 mr-2" 
          style={{width: 30}}>
          <button
            type="button"
            className="blank-button d-flex align-items-center justify-content-center"
            onClick={toggleMenu}
          >
            <i className="fas fa-ellipsis-v" style={{ fontSize: "1em" }}></i>
          </button>
          <div
            className={`dropdown-menu ${isActive ? "d-block" : "d-none"}`}
            style={{ top: 25, right: 10, left: "auto" }}
          >
            <button
              className="dropdown-item mb-1 small-menu"
              onClick={changeStatus}
            >
              Set
              <span style={{ fontWeight: 500 }}>
                {` "${
                  line.status === TasksStatusType.NY
                    ? TasksStatusType.ON_PROGRESS
                    : line.status === TasksStatusType.ON_PROGRESS
                    ? TasksStatusType.DONE
                    : TasksStatusType.ON_PROGRESS
                }"`}
              </span>
            </button>
            {/* todo : jika EC tapi bukan punya dia ga boleh approved */}
            {user?.privilege &&
              user?.privilege?.toLowerCase() !== MEMBER.toLowerCase() && (
                <button
                  className={`dropdown-item mb-1 ${
                    line.creator === true ? "d-block" : "d-none"
                  } small-menu`}
                  onClick={changeApproved}
                >
                  Set
                  <span style={{ fontWeight: 500 }}>
                    {` "${!line.approved ? "Approved" : "Not Approved"}"`}
                  </span>
                </button>
              )}
            <button
              className={`dropdown-item mb-1 ${
                line.evidence ? "" : "d-none"
              } small-menu text-danger`}
              onClick={deleteEvidence}
            >
              Delete Evidence
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
