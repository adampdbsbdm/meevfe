import React, { useState, useEffect } from "react";

import * as RoleStatusType from "../types/RoleStatus";
import SingleDataSelectInput from "./SingleDataSelectInput";
import swal, { getConfirmConfig } from "./Alert";
import styles from "./TextOutput.module.scss";

export default function RoleLine({
  id,
  username,
  name,
  role,
  fullline,
  onDelete,
  onChange,
  onUpdate,
  editMode,
  onEditModeChange,
  onChangePassword,
  onShowDetails,
}) {
  const [line, setLine] = useState({
    ...fullline,
    id,
    username,
    name,
    role,
  });
  const [prevline, setPrevline] = useState();

  useEffect(() => {
    setLine({ ...fullline, id, username, name, role });
    setPrevline({ ...fullline, id, username, name, role });
  }, []);

  async function deleteLine() {
    const result = await swal.fire(
      getConfirmConfig("Delete User?", "Do you want to delete user audiences?")
    );
    if (result.isConfirmed) {
      onDelete(line.id);
    }
  }

  async function updateLine() {
    await onUpdate(line.id);
    const newline = {
      ...line,
      isEdit: false,
    };
    setLine(newline);
    setPrevline(newline);
    onChange(newline);
    onEditModeChange(-1);
  }

  function enableEdit() {
    setPrevline({ ...line });
    const newline = {
      ...line,
      isEdit: true,
    };
    setLine(newline);
    onChange(newline);
    onEditModeChange(1);
  }

  function undoEdit() {
    const prev = { ...prevline, isEdit: false };
    setLine(prev);
    onChange(prev);
    onEditModeChange(-1);
  }

  function roleChanged(e) {
    const newline = {
      ...line,
      privilege: e.value,
      role: { value: e.value, label: e.label },
    };
    setLine(newline);
    onChange(newline);
  }

  return (
    <div className="row mx-0 col-12 py-4 px-0 row-list">
      <div className="row mx-0 col-10">
        <div className="col-4 d-flex align-items-center">
          <p className={`${styles.textcontent}`}>{username}</p>
        </div>
        <div className="col-4 d-flex align-items-center">
          {!editMode ? (
            <p className={`${styles.textcontent}`}>{name}</p>
          ) : (
            <>
              <input
                className="form-control"
                id={`name${id}`}
                type="text"
                name={`name${id}`}
                onChange={(e) => {
                  const newline = { ...line, name: e.target.value };
                  setLine(newline);
                  onChange(newline);
                }}
                value={name}
                required
              />
              <div className="invalid-feedback">Field is required</div>
            </>
          )}
        </div>
        <div className="col-4 d-flex align-items-center">
          {!editMode ? (
            <p className={`${styles.textcontent}`}>{role.value}</p>
          ) : (
            <>
              <SingleDataSelectInput
                name="role"
                placeholder="Select role..."
                value={role}
                change={roleChanged}
                required="true"
                availableOptions={RoleStatusType.ROLE_ALL_STATUS_LIST.map(
                  (o, i) => {
                    return { value: o, label: o };
                  }
                )}
              ></SingleDataSelectInput>
            </>
          )}
        </div>
      </div>
      <div className="col-2 row">
        {!editMode ? (
          <>
            <button
              type="button"
              title="Delete"
              key="gantidong-delete"
              className="blank-button text-danger 
                d-flex align-items-center justify-content-center mx-2 my-1 px-0"
              onClick={deleteLine}
            >
              <i className="fas fa-trash-alt" style={{ fontSize: "1em" }}></i>
            </button>
            <button
              type="button"
              title="Edit"
              key="gantidong-edit"
              className="blank-button text-secondary 
                d-flex align-items-center justify-content-center mx-2 my-1 px-0"
              onClick={enableEdit}
            >
              <i className="fas fa-edit" style={{ fontSize: "1em" }}></i>
            </button>
            <button
              type="button"
              title="Change Password"
              key="gantidong-cp"
              className="blank-button text-secondary 
                d-flex align-items-center justify-content-center mx-2 my-1 px-0"
              onClick={() => onChangePassword(line.id)}
            >
              <i className="fas fa-key" style={{ fontSize: "1em" }}></i>
            </button>
            <button
              type="button"
              title="Info"
              key="gantidong-info"
              className="blank-button text-secondary 
                d-flex align-items-center justify-content-center mx-2 my-1 px-0"
              onClick={() => onShowDetails(line.id)}
            >
              <i className="fas fa-info" style={{ fontSize: "1em" }}></i>
            </button>
          </>
        ) : (
          <>
            <button
              type="button"
              title="Update"
              key="gantidong-update"
              className="blank-button text-success 
                d-flex align-items-center justify-content-center mx-2 my-1 px-0"
              onClick={updateLine}
            >
              <i className="fa fa-check" style={{ fontSize: "1em" }}></i>
            </button>
            <button
              type="button"
              title="Cancel"
              key="gantidong-cancel"
              className="blank-button text-secondary 
                d-flex align-items-center justify-content-center mx-2 my-1 px-0"
              onClick={undoEdit}
            >
              <i className="fas fa-times" style={{ fontSize: "1em" }}></i>
            </button>
          </>
        )}
      </div>
    </div>
  );
}
