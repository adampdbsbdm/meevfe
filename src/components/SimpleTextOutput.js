import React, { Fragment } from "react";
import styles from "./TextOutput.module.scss";

export default function SimpleTextOutput({
  caption,
  value,
  link,
  spacing = [2, 10],
}) {
  return (
    <Fragment>
      <p className={`col-sm-12 col-md-${spacing[0]} ${styles.textlabel}`}>
        {caption}
      </p>
      <div className={`col-sm-12 col-md-${spacing[1]}`}>
        {!link ? (
          <p className={styles.textcontent}>{value}</p>
        ) : (
          <a className={styles.urlcontent} href={value}>
            {value}
          </a>
        )}
      </div>
    </Fragment>
  );
}
