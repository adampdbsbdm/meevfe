import React, { Fragment } from "react";
import nodata from "../assets/empty-folder.png";

export default function NoData({className}) {
  return (
    <div className={`d-flex flex-column align-items-center ${className}`}>
      <img src={nodata} width={100} />
      <p style={{ color: "black", fontWeight: "600" }}>No Data</p>
    </div>
  );
}
