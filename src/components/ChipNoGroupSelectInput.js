import React, { Fragment, useState, useEffect } from "react";

import chroma from "chroma-js";
import Select from "react-select";

export default function ChipNoGroupSelectInput({
  caption,
  name,
  value,
  availableOptions,
  change,
  placeholder,
  required,
}) {
  const [selectOptions, setSelectOptions] = useState(availableOptions);

  const [thevalue, setTheValue] = useState();

  useEffect(() => {
    const curval = value?.map((o) => {
      return { value: o.id, label: o.name, color: o.color || "#4B4B4D" };
    });
    setTheValue(curval);
  }, [value]);

  const colourStyles = {
    control: (styles) => ({
      ...styles,
      backgroundColor: "white",
    }),
    option: (styles, { data, isDisabled, isFocused, isSelected }) => {
      data.color = data.color || "#4B4B4D";
      const color = chroma(data.color);
      return {
        ...styles,

        fontSize: "0.9em",
        backgroundColor: isSelected
          ? data.color
          : isFocused
          ? color.alpha(0.1).css()
          : null,
        color: isSelected
          ? chroma.contrast(color, "white") > 2
            ? "white"
            : "black"
          : data.color,

        ":active": {
          ...styles[":active"],
          backgroundColor:
            !isDisabled && (isSelected ? data.color : color.alpha(0.3).css()),
        },
      };
    },
    multiValue: (styles, { data }) => {
      data.color = data.color || "#4B4B4D";
      const color = chroma(data.color);
      return {
        ...styles,
        borderRadius: 40,
        overflow: "hidden",
        backgroundColor: color.alpha(0.1).css(),
      };
    },
    multiValueLabel: (styles, { data }) => ({
      ...styles,
      color: data.color || "#4B4B4D",
    }),
    multiValueRemove: (styles, { data }) => ({
      ...styles,
      color: data.color || "#4B4B4D",
      ":hover": {
        backgroundColor: data.color || "#4B4B4D",
        color: "white",
      },
    }),
  };

  function updateValue(e) {
    console.log(e);
    const selected = e?.map((o) => {
      return { value: o.value, label: o.label };
    });
    setTheValue(selected);
    console.log(selected);
    change(selected);
  }

  return (
    <Fragment>
      {caption ? (
        <label
          className="col-sm-12 col-md-2"
          style={{ marginTop: "0.5em" }}
          htmlFor={name}
        >
          {caption}
          {required ? <span className="text-danger ml-0">*</span> : ""}
        </label>
      ) : (
        ""
      )}

      <div className={`px-0 col-sm-12 ${caption ? "col-md-10" : ""}`}>
        <Select
          isMulti
          options={selectOptions}
          styles={colourStyles}
          name="colors"
          placeholder={placeholder}
          className="basic-multi-select"
          classNamePrefix="select"
          onChange={updateValue}
          value={thevalue}
        />
        {required && (!thevalue || thevalue?.length === 0) ? (
          <div className="invalid-feedback custom-invalid">
            Field is required
          </div>
        ) : (
          ""
        )}
      </div>
    </Fragment>
  );
}
