import React, { Fragment, useState, useEffect } from "react";

export default function SimpleTextInput({
  caption,
  name,
  value,
  change,
  placeholder,
  required,
  className,
  tipe = "text",
  spacing = [2, 10]
}) {
  const [thevalue, setThevalue] = useState('');

  // useEffect(() => {
  //   setThevalue(value);
  // }, []);

  useEffect(() => {
    setThevalue(value);
  }, [value]);

  function onChange(e) {
    setThevalue(e.target.value);
    change({ key: name, value: e.target.value });
  }

  return (
    <Fragment>
      <label
        className={`col-sm-12 col-md-${spacing[0]} ${className}`}
        style={{ marginTop: "0.5em" }}
        htmlFor={name}
      >
        {caption}
        {required ? <span className="text-danger ml-0">*</span> : ""}
      </label>
      <div className={`col-sm-12 col-md-${spacing[1]}`}>
        <input
          className="form-control"
          id={name}
          type={tipe}
          name={name}
          onChange={onChange}
          value={thevalue}
          placeholder={placeholder}
          required={required}
        />
        {required ? (
          <div className="invalid-feedback">Field is required</div>
        ) : (
          ""
        )}
      </div>
    </Fragment>
  );
}
