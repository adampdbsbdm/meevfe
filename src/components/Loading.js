import React, { Fragment } from "react";
// import styles from "./Loading.module.scss";

export default function LoadingScreen({ loading }) {
  return (
    <Fragment>
      {loading && (
        <div className="cloading">
          <div className="cspinner spinner-border m-5" role="status">
            <span className="sr-only">Loading...</span>
          </div>
        </div>
      )}
    </Fragment>
  );
}
