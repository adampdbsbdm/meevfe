import React, { Fragment, useState, useEffect } from "react";

export default function SimpleTextInputGroup({
  caption,
  name,
  value,
  change,
  placeholder,
  required,
  icon,
  tipe = "text",
}) {
  const [thevalue, setThevalue] = useState("");

  // useEffect(() => {
  //   setThevalue(value);
  // }, []);

  useEffect(() => {
    setThevalue(value);
  }, [value]);

  function onChange(e) {
    setThevalue(e.target.value);
    change({ key: name, value: e.target.value });
  }

  return (
    <Fragment>
      <div className="input-group" style={{ marginTop: "0.5em" }}>
        <div className="input-group-prepend">
          <span className="input-group-text">
            <i className={`${icon}`} style={{ fontSize: "1em" }}></i>
          </span>
        </div>
        <input
          className="form-control"
          id={name}
          type={tipe}
          name={name}
          onChange={onChange}
          value={thevalue}
          placeholder={placeholder}
          required={required}
        />
        {required ? (
          <div className="invalid-feedback">Field is required</div>
        ) : (
          ""
        )}
      </div>
    </Fragment>
  );
}
