import React, { Fragment } from "react";
import styles from "./SummaryCardSimp.module.scss";

export default function SummaryCard({ caption, value, image, color, mcorr }) {
  return (
    <div
      className={`d-flex flex-column align-items-center py-0 justify-content-between rounded w-100`}
      style={{
        minHeight: 40,
      }}
    >
      <div
        className={`w-100 align-items-center d-flex justify-content-center rounded mb-1 p-1 text-center`}
        style={{
          backgroundColor: color,
          minHeight: 20,
        }}
      >
        <p className={`${styles.title}`}>{caption}</p>
      </div>
      <div
        className={`w-100 align-items-center d-flex justify-content-center rounded`}
        style={{
          backgroundColor: "white",
          minHeight: 20,
        }}
      >
        <p className={`font-weight-bold ${styles.value}`}>{value}</p>
      </div>
    </div>
  );
}
