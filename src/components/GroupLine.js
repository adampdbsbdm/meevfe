import React, { useState, useEffect } from "react";

import * as RoleStatusType from "../types/RoleStatus";
import ChipNoGroupSelectInput from "./ChipNoGroupSelectInput";
import SingleDataSelectInput from "./SingleDataSelectInput";
import swal, { getConfirmConfig } from "./Alert";
import styles from "./TextOutput.module.scss";
import TextPill from "./TextPill";
import { AUDCOLOR } from "../types/ColorStatus";

export default function GroupLine({
  id,
  name,
  description,
  members,
  availableName,
  onDelete,
  onChange,
  onUpdate,
  editMode,
  onEditModeChange,
}) {
  const [line, setLine] = useState({
    id: "",
    id_person: "",
    name_person: "",
    role: "",
  });
  const [prevline, setPrevline] = useState();

  useEffect(() => {
    setLine({ id, name, description, members });
    setPrevline({ id, name, description, members });
  }, []);

  function memberChanged(e) {
    const newline = {
      ...line,
      members: e.map((v) => ({ id: v.value, name: v.label })),
    };

    setLine(newline);
    onChange(newline);
  }

  async function deleteLine() {
    const result = await swal.fire(
      getConfirmConfig(
        "Delete Group?",
        "Do you want to delete group audiences?"
      )
    );
    if (result.isConfirmed) {
      onDelete(line.id);
    }
  }

  async function updateLine() {
    await onUpdate(line.id);
    const newline = {
      ...line,
      isEdit: false,
    };
    setLine(newline);
    setPrevline(newline);
    onChange(newline);
    onEditModeChange(-1);
  }

  function enableEdit() {
    setPrevline({ ...line });
    const newline = {
      ...line,
      isEdit: true,
    };
    setLine(newline);
    onChange(newline);
    onEditModeChange(1);
  }

  function undoEdit() {
    const prev = { ...prevline, isEdit: false };
    setLine(prev);
    onChange(prev);
    onEditModeChange(-1);
  }

  return (
    <div className="row mx-0 col-12 py-4 px-0 row-list">
      <div className="row mx-0 col-11">
        <div className="col-4 d-flex align-items-center">
          {!editMode ? (
            <p className={`${styles.textcontent}`}>{name}</p>
          ) : (
            <>
              <input
                className="form-control"
                id={`groupname${id}`}
                type="text"
                name={`groupname${id}`}
                onChange={(e) => {
                  const newline = { ...line, name: e.target.value };
                  setLine(newline);
                  onChange(newline);
                }}
                value={name}
                required
              />
              <div className="invalid-feedback">Field is required</div>
            </>
          )}
        </div>
        <div className="col-4 d-flex align-items-center">
          {!editMode ? (
            <p className={`${styles.textcontent}`}>{description || "-"}</p>
          ) : (
            <textarea
              className="form-control"
              id={`groupdesc${id}`}
              type="text"
              name={`groupdesc${id}`}
              onChange={(e) => {
                const newline = { ...line, description: e.target.value };
                setLine(newline);
                onChange(newline);
              }}
              value={description}
              required
              rows={1}
            />
          )}
        </div>
        <div className="col-4 d-flex align-items-center">
          {!editMode ? (
            <div className="row mx-0">
              {members?.map((v, i) => {
                return (
                  <TextPill value={v.name} key={i} bgcolor="#ededed"/>
                );
              })}
            </div>
          ) : (
            <div>
              <ChipNoGroupSelectInput
                name="groupmembers"
                placeholder="Select member..."
                value={members}
                change={memberChanged}
                required="true"
                availableOptions={availableName}
              ></ChipNoGroupSelectInput>
              <div className="invalid-feedback">Field is required</div>
            </div>
          )}
        </div>
      </div>
      <div className="col-1 row">
        {!editMode ? (
          <>
            <button
              type="button"
              title="Delete"
              key="gantidong-delete"
              className="blank-button text-danger 
                d-flex align-items-center justify-content-center mx-2 my-1 px-0"
              onClick={deleteLine}
            >
              <i className="fas fa-trash-alt" style={{ fontSize: "1em" }}></i>
            </button>
            <button
              type="button"
              title="Edit"
              key="gantidong-edit"
              className="blank-button text-secondary 
                d-flex align-items-center justify-content-center mx-2 my-1 px-0"
              onClick={enableEdit}
            >
              <i className="fas fa-edit" style={{ fontSize: "1em" }}></i>
            </button>
          </>
        ) : (
          <>
            <button
              type="button"
              title="Update"
              key="gantidong-update"
              className="blank-button text-success 
                d-flex align-items-center justify-content-center mx-2 my-1 px-0"
              onClick={updateLine}
            >
              <i className="fa fa-check" style={{ fontSize: "1em" }}></i>
            </button>
            <button
              type="button"
              title="Cancel"
              key="gantidong-cancel"
              className="blank-button text-secondary 
                d-flex align-items-center justify-content-center mx-2 my-1 px-0"
              onClick={undoEdit}
            >
              <i className="fas fa-times" style={{ fontSize: "1em" }}></i>
            </button>
          </>
        )}
      </div>
    </div>
  );
}
