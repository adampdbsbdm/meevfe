import React, { useState } from "react";
import styles from "./NotificationLine.module.scss";
import * as ActivityStatusType from "../types/ActivityStatus";

export default function NotificationLine({ type, content, date }) {
  return (
    <div
      className={`row flex-column mx-0 px-1 py-2 border-bottom pointer ${styles.parent}`}
      style={{ width: 325 }}
    >
      <div className="col-12 d-flex justify-content-between">
        <span className={`${styles.title}`}>
          {type === ActivityStatusType.MEETING
            ? "Meeting Invitation"
            : type === ActivityStatusType.LEADER
            ? "Meeting Leader"
            : type === ActivityStatusType.TASK
            ? "Task To Do"
            : "PIC Agenda"}
        </span>
        <span className={`${styles.date}`}>{date}</span>
      </div>
      <span className={`${styles.content} col-12`}>{content}</span>
    </div>
  );
}
