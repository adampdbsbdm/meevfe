import React, { Fragment, useState, useEffect } from "react";
import SimpleDatetimePicker from "./SimpleDatetimePicker";
import SingleDataSelectInput from "./SingleDataSelectInput";

export default function DecisionLine({
  id,
  pic,
  decision,
  detail,
  due,
  availableName,
  onDelete,
  onChange,
}) {
  const [line, setLine] = useState({
    id: "",
    pic: {},
    decision: "",
    detail: "",
    due: "",
  });

  useEffect(() => {
    setLine({ id, pic, decision, detail, due });
  }, []);

  function updateValue(newline) {
    setLine(newline);
    onChange(newline);
  }

  function memberChanged(e) {
    const newline = { ...line, pic: { id: e.value, name: e.label } };
    setLine(newline);
    onChange(newline);
  }

  function deleteLine() {
    onDelete(line.id);
  }

  return (
    <div className="row mx-0 col-12 py-4 px-0 row-list">
      <div className="row mx-0 col-11">
        <div className="col-3">
          <SingleDataSelectInput
            name="pic"
            placeholder="Select pic name..."
            value={pic}
            change={memberChanged}
            required="true"
            availableOptions={availableName.map((o, i) => {
              return { value: o.value, label: o.label };
            })}
          ></SingleDataSelectInput>
        </div>

        <div className="col-3">
          <input
            className="form-control"
            id={`pointdec${id}`}
            type="text"
            name={`pointdec${id}`}
            onChange={(e) => {
              const newline = {
                ...line,
                pic: { id: pic.value, name: pic.label },
                decision: e.target.value,
              };
              updateValue(newline);
            }}
            value={line.decision}
            required
          />
          <div className="invalid-feedback">Field is required</div>
        </div>

        <div className="col-3">
          <textarea
            className="form-control"
            id={`detaildec${id}`}
            type="text"
            name={`detaildec${id}`}
            onChange={(e) => {
              console.log(pic);
              const newline = {
                ...line,
                pic: { id: pic.value, name: pic.label },
                detail: e.target.value,
              };
              updateValue(newline);
            }}
            value={line.detail}
            rows="1"
            required
          />
          <div className="invalid-feedback">Field is required</div>
        </div>

        <div className="col-3">
          <SimpleDatetimePicker
            name="due"
            caption=""
            placeholder="Select datetime"
            value={due}
            change={(e) => {
              const newline = {
                ...line,
                pic: { id: pic.value, name: pic.label },
                due: e.value,
              };
              updateValue(newline);
            }}
            disableTime="true"
            required="true"
          ></SimpleDatetimePicker>
        </div>
      </div>
      <button
        type="button"
        className="blank-button text-danger d-flex align-items-center justify-content-center col-1"
        onClick={deleteLine}
      >
        <i className="fas fa-trash-alt" style={{ fontSize: "1em" }}></i>
      </button>
    </div>
  );
}
