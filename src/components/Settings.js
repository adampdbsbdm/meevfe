import React, { Fragment, useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import swal from "../components/Alert";
import { clearUserDataStorage } from "../data/user";
import ChangeAccount from "../pages/ChangeAccount";
import ChangePassword from "../pages/ChangePassword";

export default function Settings({ logoutCallback }) {
  const [isActive, setActive] = useState(false);

  const toggleMenu = () => {
    setActive(!isActive);
  };

  const disableMenu = (e) => {
    if (isActive) {
      setActive(false);
    }
    window.removeEventListener("click", disableMenu);
  };

  // ref : https://stackoverflow.com/questions/57944794/react-hooks-setstate-does-not-update-state-properties
  useEffect(() => {
    // window.addEventListener("click", disableMenu);
    // return () => window.removeEventListener("click", disableMenu);
    setTimeout(() => {
      if (isActive) {
        window.addEventListener("click", disableMenu);
      }
    }, 0);
  });

  return (
    <div className="dropdown ml-2">
      {/* <button
        className="btn btn-secondary dropdown-toggle"
        type="button"
      >
        <i className="fas fa-cog" style={{ fontSize: "21px" }}></i>
      </button> */}
      <button
        type="button"
        className="blank-button d-flex align-items-center"
        onClick={toggleMenu}
      >
        <i className="fas fa-cog" style={{ fontSize: "21px" }}></i>
      </button>
      <div
        className={`dropdown-menu ${isActive ? "d-block" : "d-none"}`}
        style={{ top: 25, right: 10, left: "auto" }}
      >
        <ChangeAccountItem
          className={"blank-button dropdown-item"}
          callback={disableMenu}
        ></ChangeAccountItem>
        <ChangePasswordItem
          className={"blank-button dropdown-item"}
          callback={disableMenu}
        ></ChangePasswordItem>
        <div className="dropdown-divider"></div>
        <LogoutItem
          className={"blank-button dropdown-item"}
          callback={logoutCallback}
        ></LogoutItem>
      </div>
    </div>
  );
}

function LogoutItem({ className, callback }) {
  let history = useHistory();

  const warnLogout = async () => {
    const result = await swal.fire({
      title: "Logout?",
      text: "You can login again after logout",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#d33",
      cancelButtonColor: "#cccdd0",
      confirmButtonText: "Yes",
    });
    if (result.isConfirmed) {
      clearUserDataStorage();
      callback();
      window.location.reload();
      // history.push('/dashboard')
    }
  };

  return (
    <button className={`${className}`} onClick={warnLogout}>
      Logout
    </button>
  );
}

function ChangePasswordItem({ className, callback }) {
  const [cpShow, setCPShow] = useState(false);

  return (
    <Fragment>
      <button className={`${className}`} onClick={() => setCPShow(true)}>
        Change Password
      </button>
      <ChangePassword
        isShow={cpShow}
        onChange={setCPShow}
        onSuccess={callback}
      />
    </Fragment>
  );
}

function ChangeAccountItem({ className, callback }) {
  const [caShow, setCAShow] = useState(false);

  return (
    <Fragment>
      <button className={`${className}`} onClick={() => setCAShow(true)}>
        Account
      </button>
      <ChangeAccount
        isShow={caShow}
        onChange={setCAShow}
        onSuccess={callback}
      />
    </Fragment>
  );
}
