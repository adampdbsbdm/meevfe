
import { createContext } from "react";
import { MEMBER } from "../types/RoleStatus";

export const UserContext = createContext();

export function isLoggedIn() {
    // Sementara pakai localstorage
    return localStorage.getItem("access_token") ? true : false
}

export function isNonMember() {
    const user = getUser()
    return user?.privilege && user?.privilege?.toLowerCase() !== MEMBER.toLocaleLowerCase() ? true : false
}

export function getUser() {
    // Sementara pakai localstorage
    try {
       const user = JSON.parse(localStorage.getItem("user"))
       return user
    } catch {
        return null
    }
}

export function getToken() {
    // Sementara pakai localstorage
    return localStorage.getItem("access_token")
}

export function setUserDataStorage(data) {
    // Sementara pakai localstorage
    localStorage.setItem("user", JSON.stringify(data?.payload?.user))
    localStorage.setItem("access_token", data?.token)
}

export function setUserDataStorageOnly(data) {
    // Sementara pakai localstorage
    localStorage.setItem("user", JSON.stringify(data))
}

export function clearUserDataStorage() {
    // Sementara pakai localstorage
    localStorage.clear()
}
