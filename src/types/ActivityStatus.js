export const LEADER = "Leader";
export const MEETING = "Meeting";
export const INVITATION = "Invitation";
export const PIC = "PIC";
export const TASK = "Task";
export const ACTIVITY_STATUS_LIST = [LEADER, MEETING, PIC, TASK, INVITATION];