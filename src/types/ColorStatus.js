export const PERSON = "#4B4B4D";
export const GROUP = "#36B37E";
export const EXTERNAL = "#00B8D9";
export const AUDCOLOR = { person: PERSON, group: GROUP, external: EXTERNAL };

export const leaderColor = "#c7b699"; //#321FDB";
export const invitationColor = "#414754"; //#3399FF";
export const picColor = "#82807F"; // "#F9B115";
export const taskColor = "#aaaeb5"; //"#E55353";
export const meetingColor = "#3788d8";
export const DASHCOLOR = {
  leader: leaderColor,
  invitation: invitationColor,
  task: taskColor,
  pic: picColor,
  meeting: meetingColor,
};
