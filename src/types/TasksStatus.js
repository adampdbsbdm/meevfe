export const DONE = "Done";
export const NY = "Not Yet";
export const ON_PROGRESS = "On Progress";
export const TASKS_STATUS_LIST = [DONE, NY, ON_PROGRESS];
