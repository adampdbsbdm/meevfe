import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import Moment from "moment";
import momentLocalizer from "react-widgets-moment";

// For dropdown
// import $ from 'jquery';
// import Popper from '@popperjs/core';
// import 'bootstrap/dist/js/bootstrap.bundle.min';

Moment.locale("en");
momentLocalizer();

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
